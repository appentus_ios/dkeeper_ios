//
//  Helper.swift
//  Ezyride!
//
//  Created by Appentus on 04/04/18.
//  Copyright © 2018 love. All rights reserved.
//

import UIKit
import Foundation
import Toast_Swift

var alertVC: CPAlertVC!
var settingCondition = true
var detailCondition = true

func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
        completion()
    }
}

func transistionLeft(View:UIView){
    
    let transition = CATransition()
    transition.duration = 0.45
    transition.type = CATransitionType.push
    transition.subtype = CATransitionSubtype.fromLeft
    View.window!.layer.add(transition, forKey: kCATransition)
    
    
    
}
func transistionRight(View:UIView){
    
    
    let transition = CATransition()
    transition.duration = 0.45
    transition.type = CATransitionType.push
    transition.subtype = CATransitionSubtype.fromRight
    View.window!.layer.add(transition, forKey: kCATransition)
    
    
}

func anyConvertToDictionary(text: String) -> [String: Any]? {
    
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
            
        }
    }
    return nil
}

func springAnimation(view:UIView){
    
    
    view.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
    
    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.0, options: .allowUserInteraction, animations: {view.transform = .identity}, completion: nil)
    
}

func CircularMask(view:UIView){
    
    view.layer.cornerRadius = view.frame.width/2
    view.layer.masksToBounds = true
    
}


func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}


extension UIViewController:UIViewControllerTransitioningDelegate{
    
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let transition = CircularTransition()
        transition.transitionMode = .present
        transition.startingPoint = self.view.center
        transition.circleColor = UIColor.clear
        
        return transition
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let transition = CircularTransition()
        transition.transitionMode = .dismiss
        transition.startingPoint = self.view.center
        transition.circleColor = UIColor.clear
        
        return transition
    }
    
    
    func viewPresent(storyBoard:String,viewId:String){
        
        //        self.view.center = centerPoint
        let storyboard = UIStoryboard(name: "\(storyBoard)", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "\(viewId)")
        self.navigationController?.pushViewController(vc, animated: true)
        
        //        vc.transitioningDelegate = self
        //        vc.modalPresentationStyle = .custom
        //        self.present(vc, animated: true, completion: nil)
        
    }
    
    
}
extension UIImage {
    
    func imageWithColor(color: UIColor) -> UIImage? {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}


extension UIView{
    func addConstraintsWithFormat(format:String, views: UIView...){
        
        var viewsDictionary = [String:UIView]()
        for (index, view) in views.enumerated(){
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
}

class BaseCell:UICollectionViewCell{
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        
    }
}




func isValidEmailAddress(emailAddressString: String) -> Bool {
    
    var returnValue = true
    let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
    
    do {
        let regex = try NSRegularExpression(pattern: emailRegEx)
        let nsString = emailAddressString as NSString
        let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
        
        if results.count == 0
        {
            returnValue = false
        }
        
    } catch let error as NSError {
        print("invalid regex: \(error.localizedDescription)")
        returnValue = false
    }
    
    return  returnValue
}

extension UITextField{
    func underlined(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.darkGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

extension UITextField{
    func underlinedWhite(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
//extension UILabel {
//    func underline() {
//        if let textString = self.text {
//            let attributedString = NSMutableAttributedString(string: textString)
//            attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
//            attributedText = attributedString
//        }
//    }
//}


//extension UIButton {
//    func underline() {
//        let attributedString = NSMutableAttributedString(string: (self.titleLabel?.text!)!)
//        attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: (self.titleLabel?.text!.count)!))
//        self.setAttributedTitle(attributedString, for: .normal)
//    }
//}
extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}

extension UIView{
    func showError(_ msg:String) {
        // create a new style
        var style = ToastStyle()
        style.messageColor = .white
        style.backgroundColor = .red
        ToastManager.shared.isTapToDismissEnabled = true
        ToastManager.shared.isQueueEnabled = true
        self.makeToast(msg, duration: 2.0, position: .bottom, style: style)
        
    }
    func showSuccess(_ msg:String) {
        var style = ToastStyle()
        style.messageColor = hexStringToUIColor(hex: "162B41")
        style.backgroundColor = kGreenBaseColor
        ToastManager.shared.isTapToDismissEnabled = true
        ToastManager.shared.isQueueEnabled = true
        self.makeToast(msg, duration: 2.0, position: .bottom, style: style)
        
    }
    
    
    func showLoader(){
        JJHUD.showLoading()
        self.isUserInteractionEnabled = false
    }
    
    func hideLoader() {
        self.isUserInteractionEnabled = true
        JJHUD.hide()
    }
    
}
