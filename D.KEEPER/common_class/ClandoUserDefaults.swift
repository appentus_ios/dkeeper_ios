//
//
//
//  Created by Love on 03/07/18.
//  Copyright © 2018 Appentus. All rights reserved.
//

import Foundation

@objc class ClandoUserDefaults: NSObject
{
    static let sharedInstance = ClandoUserDefaults()
    
    private override init() {
        super.init()
        self.retriveUserFromUserDefaults()
    }
    
    
    var _user_wallet_amount = String()
    
    var user_wallet_amount: String {
        get {
            return _user_wallet_amount
        }
        
        set {
            
            if _user_wallet_amount != newValue {
                _user_wallet_amount = newValue
                UserDefaults.standard.set(_user_wallet_amount, forKey: "user_wallet_amount")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    
    var _is_guest = String()
    
    var is_guest: String {
        get {
            return _is_guest
        }
        
        set {
            
            if _is_guest != newValue {
                _is_guest = newValue
                UserDefaults.standard.set(_is_guest, forKey: "is_guest")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    
    var _user_image = String()
    
    var user_image: String {
        get {
            return _user_image
        }
        
        set {
            
            if _user_image != newValue {
                _user_image = newValue
                UserDefaults.standard.set(_user_image, forKey: "user_image")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    var _user_referral_id = String()
    
    var user_referral_id: String {
        get {
            return _user_referral_id
        }
        
        set {
            
            if _user_referral_id != newValue {
                _user_referral_id = newValue
                UserDefaults.standard.set(_user_referral_id, forKey: "user_referral_id")
                UserDefaults.standard.synchronize()
            }
        }
    }
    var _AppliedForDriver = String()
    
    var AppliedForDriver: String {
        get {
            return _AppliedForDriver
        }
        
        set {
            
            if _AppliedForDriver != newValue {
                _AppliedForDriver = newValue
                UserDefaults.standard.set(_AppliedForDriver, forKey: "AppliedForDriver")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    var _user_type = String()
    
    var user_type: String {
        get {
            return _user_type
        }
        
        set {
            
            if _user_type != newValue {
                _user_type = newValue
                UserDefaults.standard.set(_user_type, forKey: "user_type")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    var _user_mobile = String()
    
    var user_mobile: String {
        get {
            return _user_mobile
        }
        
        set {
            
            if _user_mobile != newValue {
                _user_mobile = newValue
                UserDefaults.standard.set(_user_mobile, forKey: "user_mobile")
                UserDefaults.standard.synchronize()
            }
        }
    }
    var _user_mobile_code = String()
    
    var user_mobile_code: String {
        get {
            return _user_mobile_code
        }
        
        set {
            
            if _user_mobile_code != newValue {
                _user_mobile_code = newValue
                UserDefaults.standard.set(_user_mobile_code, forKey: "user_mobile_code")
                UserDefaults.standard.synchronize()
            }
        }
    }
    var _user_otp = String()
    
    var user_otp: String {
        get {
            return _user_otp
        }
        
        set {
            
            if _user_otp != newValue {
                _user_otp = newValue
                UserDefaults.standard.set(_user_otp, forKey: "user_otp")
                UserDefaults.standard.synchronize()
            }
        }
    }
    var _user_pass = String()
    
    var user_pass: String {
        get {
            return _user_pass
        }
        
        set {
            
            if _user_pass != newValue {
                _user_pass = newValue
                UserDefaults.standard.set(_user_pass, forKey: "user_pass")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    var _user_email = String()
    var user_email: String {
        get {
            return _user_email
        }
        
        set {
            
            if _user_email != newValue {
                _user_email = newValue
                UserDefaults.standard.set(_user_email, forKey: "user_email")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    var _user_name = String()
    var user_name: String {
        get {
            return _user_name
        }
        
        set {
            
            if _user_name != newValue {
                _user_name = newValue
                UserDefaults.standard.set(_user_name, forKey: "user_name")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    var _first_name = String()
    var first_name: String {
        get {
            return _first_name
        }
        
        set {
            
            if _first_name != newValue {
                _first_name = newValue
                UserDefaults.standard.set(_first_name, forKey: "first_name")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    var _last_name = String()
    var last_name: String {
        get {
            return _last_name
        }
        
        set {
            
            if _last_name != newValue {
                _last_name = newValue
                UserDefaults.standard.set(_last_name, forKey: "last_name")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    var _userId = String()
    
    var userId: String {
        get {
            return _userId
        }
        
        set {
            
            if _userId != newValue {
                _userId = newValue
                UserDefaults.standard.set(_userId, forKey: "userId")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    
    var _userBio = String()
    
    var userBio: String {
        get {
            return _userBio
        }
        
        set {
            
            if _userBio != newValue {
                _userBio = newValue
                UserDefaults.standard.set(_userBio, forKey: "userBio")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    var _gender = String()
    
    var gender: String {
        get {
            return _gender
        }
        
        set {
            
            if _gender != newValue {
                _gender = newValue
                UserDefaults.standard.set(_gender, forKey: "gender")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    var _DOB = String()
    
    var DOB: String {
        get {
            return _DOB
        }
        
        set {
            
            if _DOB != newValue {
                _DOB = newValue
                UserDefaults.standard.set(_DOB, forKey: "DOB")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    
    var _KeepLogin = String()
    
    var KeepLogin: String {
        get {
            return _KeepLogin
        }
        
        set {
            
            if _KeepLogin != newValue {
                _KeepLogin = newValue
                UserDefaults.standard.set(_KeepLogin, forKey: "KeepLogin")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    func retriveUserFromUserDefaults() {
        let standardUserDefaults = UserDefaults.standard
        _user_otp = standardUserDefaults.string(forKey: "user_otp") != nil ? standardUserDefaults.string(forKey: "user_otp")! : ""

        _user_mobile_code = standardUserDefaults.string(forKey: "user_mobile_code") != nil ? standardUserDefaults.string(forKey: "user_mobile_code")! : ""
        _last_name = standardUserDefaults.string(forKey: "last_name") != nil ? standardUserDefaults.string(forKey: "last_name")! : ""
        _first_name = standardUserDefaults.string(forKey: "first_name") != nil ? standardUserDefaults.string(forKey: "first_name")! : ""
        _userId = standardUserDefaults.string(forKey: "userId") != nil ? standardUserDefaults.string(forKey: "userId")! : ""
        _KeepLogin = standardUserDefaults.string(forKey: "KeepLogin") != nil ? standardUserDefaults.string(forKey: "KeepLogin")! : ""
        _user_email = standardUserDefaults.string(forKey: "user_email") != nil ? standardUserDefaults.string(forKey: "user_email")! : ""
        _user_name = standardUserDefaults.string(forKey: "user_name") != nil ? standardUserDefaults.string(forKey: "user_name")! : ""
        _user_mobile = standardUserDefaults.string(forKey: "user_mobile") != nil ? standardUserDefaults.string(forKey: "user_mobile")! : ""
        _user_image = standardUserDefaults.string(forKey: "user_image") != nil ? standardUserDefaults.string(forKey: "user_image")! : ""
        _user_type = standardUserDefaults.string(forKey: "user_type") != nil ? standardUserDefaults.string(forKey: "user_type")! : ""
         _gender = standardUserDefaults.string(forKey: "gender") != nil ? standardUserDefaults.string(forKey: "gender")! : ""
         _DOB = standardUserDefaults.string(forKey: "DOB") != nil ? standardUserDefaults.string(forKey: "DOB")! : ""
        _user_pass = standardUserDefaults.string(forKey: "user_pass") != nil ? standardUserDefaults.string(forKey: "user_pass")! : ""
        _user_referral_id = standardUserDefaults.string(forKey: "user_referral_id") != nil ? standardUserDefaults.string(forKey: "user_referral_id")! : ""
        _user_wallet_amount = standardUserDefaults.string(forKey: "user_wallet_amount") != nil ? standardUserDefaults.string(forKey: "user_wallet_amount")! : ""
        _is_guest = standardUserDefaults.string(forKey: "is_guest") != nil ? standardUserDefaults.string(forKey: "is_guest")! : ""

        standardUserDefaults.synchronize()
        
    }
    
    func remove_from_userdefaultds() {
        ClandoUserDefaults.sharedInstance.user_otp = ""
        ClandoUserDefaults.sharedInstance.user_mobile_code = ""
        ClandoUserDefaults.sharedInstance.last_name = ""
        ClandoUserDefaults.sharedInstance.first_name = ""
        ClandoUserDefaults.sharedInstance.userId = ""
        ClandoUserDefaults.sharedInstance.KeepLogin = ""
        ClandoUserDefaults.sharedInstance.user_email = ""
        ClandoUserDefaults.sharedInstance.user_name = ""
        ClandoUserDefaults.sharedInstance.user_mobile = ""
        ClandoUserDefaults.sharedInstance.user_image = ""
        ClandoUserDefaults.sharedInstance.user_type = ""
        ClandoUserDefaults.sharedInstance.gender = ""
        ClandoUserDefaults.sharedInstance.DOB = ""
        ClandoUserDefaults.sharedInstance.user_pass = ""
        ClandoUserDefaults.sharedInstance.user_referral_id = ""
        ClandoUserDefaults.sharedInstance.user_wallet_amount = ""
        ClandoUserDefaults.sharedInstance.is_guest = ""
        retriveUserFromUserDefaults()
    }
    
}
