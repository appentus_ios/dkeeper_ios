
//
//  functions.swift
//  spark
//
//  Created by Love on 31/10/18.
//  Copyright © 2018 Appentus. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import Alamofire

private var uuid: UUID?

func add_image_in_textField(tf : UITextField , img : UIImage){
    tf.leftViewMode = UITextField.ViewMode.always
    let imageView = UIImageView(frame: CGRect(x: 15, y: (tf.frame.size.height / 2) - 10, width: 20, height: 20))
    let image = img
    imageView.image = image
    
    let view = UIView()
    view.backgroundColor = UIColor.clear
    view.frame = CGRect(x: 0.0, y: 0.0, width: 50, height: tf.frame.size.height)
    
    view.addSubview(imageView)
    view.bringSubviewToFront(imageView)
    
    tf.leftView = view
}

func shadow_on_view(view : UIView, color: UIColor, corner_radius : CGFloat) {
    view.layer.cornerRadius = corner_radius
    view.layer.shadowColor = color.cgColor
    view.layer.shadowOpacity = 0.5
    view.layer.shadowOffset = CGSize.zero
    view.layer.shadowRadius = 5
}
func shadow_on_Text_view(view : UITextView, color: UIColor, corner_radius : CGFloat) {
    view.layer.cornerRadius = corner_radius
    view.layer.shadowColor = color.cgColor
    view.layer.shadowOpacity = 0.5
    view.layer.shadowOffset = CGSize.zero
    view.layer.shadowRadius = 5
}

func shadow_on_button(view : UIButton, color: UIColor, corner_radius : CGFloat) {
    view.layer.cornerRadius = corner_radius
    view.layer.shadowColor = color.cgColor
    view.layer.shadowOpacity = 0.5
    view.layer.shadowOffset = CGSize.zero
    view.layer.shadowRadius = 5
}
extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

func shadow_only_for_view(view : UIView, color: UIColor){
    view.layer.shadowColor = color.cgColor
    view.layer.shadowOpacity = 0.5
    view.layer.shadowOffset = CGSize.zero
    view.layer.shadowRadius = 5
}

func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String , completion : @escaping (String)->()) {
    var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
    let lat: Double = Double("\(pdblLatitude)")!
    //21.228124
    let lon: Double = Double("\(pdblLongitude)")!
    //72.833770
    let ceo: CLGeocoder = CLGeocoder()
    center.latitude = lat
    center.longitude = lon
    
    let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
    
    getAddressFromLatLong(latitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude) { (response) in
        completion(response)
    }
//    ceo.reverseGeocodeLocation(loc, completionHandler:
//        {(placemarks, error) in
//            if (error != nil)
//            {
//                print("reverse geodcode fail: \(error!.localizedDescription)")
//                completion("reverse geodcode fail: \(error!.localizedDescription)")
//            }
//            let pm = placemarks! as [CLPlacemark]
//
//            if pm.count > 0 {
//                let pm = placemarks![0]
////                print(pm.country)
////                print(pm.locality)
////                print(pm.subLocality)
////                print(pm.thoroughfare)
////                print(pm.postalCode)
////                print(pm.subThoroughfare)
//                var addressString : String = ""
//                if pm.subLocality != nil {
//                    addressString = addressString + pm.subLocality! + ", "
//                }
//                if pm.thoroughfare != nil {
//                    addressString = addressString + pm.thoroughfare! + ", "
//                }
//                if pm.locality != nil {
//                    addressString = addressString + pm.locality! + ", "
//                }
//                if pm.country != nil {
//                    addressString = addressString + pm.country! + ", "
//                }
//                if pm.postalCode != nil {
//                    addressString = addressString + pm.postalCode! + " "
//                }
//
//
//                print(addressString)
//                completion(addressString)
//            }
//    })
    
    
}

func getAddressFromLatLong(latitude: Double, longitude : Double, completion: @escaping (String)->()) {
    let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=AIzaSyCl8EQNpuwCmYy_JAhP_sTfas_sxCSr3_s"
    
    Alamofire.request(url).validate().responseJSON { response in
        switch response.result {
        case .success:
            
            let responseJson = response.result.value! as! NSDictionary
            
            if let results = responseJson.object(forKey: "results")! as? [NSDictionary] {
                if results.count > 0 {
                    if let addressComponents = results[0]["address_components"]! as? [NSDictionary] {
                        var address = results[0]["formatted_address"] as? String
                        completion(address!)
//                        for component in addressComponents {
//                            if let temp = component.object(forKey: "types") as? [String] {
//                                if (temp[0] == "postal_code") {
//                                    self.pincode = component["long_name"] as? String
//                                }
//                                if (temp[0] == "locality") {
//                                    self.city = component["long_name"] as? String
//                                }
//                                if (temp[0] == "administrative_area_level_1") {
//                                    self.state = component["long_name"] as? String
//                                }
//                                if (temp[0] == "country") {
//                                    self.country = component["long_name"] as? String
//                                }
//                            }
//                        }
                    }
                }
            }
        case .failure(let error):
            print(error)
            completion("\(error)")
        }
    }
}





func getImageFromWeb(_ urlString: String, closure: @escaping (UIImage?) -> ()) {
    guard let url = URL(string: urlString) else {
        return closure(nil)
    }
    let task = URLSession(configuration: .default).dataTask(with: url) { (data, response, error) in
        guard error == nil else {
            print("error: \(String(describing: error))")
            return closure(nil)
        }
        guard response != nil else {
            print("no response")
            return closure(nil)
        }
        guard data != nil else {
            print("no data")
            return closure(nil)
        }
        DispatchQueue.main.async {
            closure(UIImage(data: data!))
        }
    }; task.resume()
}

func phone(phoneNum: String) {
    if let url = URL(string: "tel://\(phoneNum)") {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url as URL)
        }
    }
}

//func draw(src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D, mapView:GMSMapView){
//
//    let config = URLSessionConfiguration.default
//    let session = URLSession(configuration: config)
//
//    let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(src.latitude),\(src.longitude)&destination=\(dst.latitude),\(dst.longitude)&sensor=false&mode=walking&key=AIzaSyCl8EQNpuwCmYy_JAhP_sTfas_sxCSr3_s")!
//
//    let task = session.dataTask(with: url, completionHandler: {
//        (data, response, error) in
//        if error != nil {
//            print(error!.localizedDescription)
//        } else {
//            do {
//                if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
//
//                    let preRoutes = json["routes"] as! NSArray
//                    let routes = preRoutes[0] as! NSDictionary
//                    let routeOverviewPolyline:NSDictionary = routes.value(forKey: "overview_polyline") as! NSDictionary
//                    let polyString = routeOverviewPolyline.object(forKey: "points") as! String
//
//                    DispatchQueue.main.async(execute: {
//                        let path = GMSPath(fromEncodedPath: polyString)
//                        let polyline = GMSPolyline(path: path)
//                        polyline.strokeWidth = 5.0
//                        polyline.strokeColor = APP_COLOR
//                        polyline.map = mapView
//                    })
//                }
//
//            } catch {
//                print("parsing error")
//            }
//        }
//    })
//    task.resume()
//}



extension UIViewController {
    func present_VC(vc_name: String, storybord_name : String) {
        let story = UIStoryboard(name: storybord_name, bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: vc_name)
        self.present(vc, animated: true, completion: nil)
    }
    
    func push_VC(vc_name: String, storybord_name : String) {
        let story = UIStoryboard(name: storybord_name, bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: vc_name)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func dismiss_VC() {
        self.navigationController?.popViewController(animated: true)
    }
    
}



enum CardType: String {
    case Unknown, Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay
    
    static let allCards = [Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay]
    
    var regex : String {
        switch self {
        case .Amex:
            return "^3[47][0-9]{5,}$"
        case .Visa:
            return "^4[0-9]{6,}([0-9]{3})?$"
        case .MasterCard:
            return "^(5[1-5][0-9]{4}|677189)[0-9]{5,}$"
        case .Diners:
            return "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
        case .Discover:
            return "^6(?:011|5[0-9]{2})[0-9]{3,}$"
        case .JCB:
            return "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
        case .UnionPay:
            return "^(62|88)[0-9]{5,}$"
        case .Hipercard:
            return "^(606282|3841)[0-9]{5,}$"
        case .Elo:
            return "^((((636368)|(438935)|(504175)|(451416)|(636297))[0-9]{0,10})|((5067)|(4576)|(4011))[0-9]{0,12})$"
        default:
            return ""
        }
    }
}



func matchesRegex(regex: String!, text: String!) -> Bool {
    do {
        let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
        let nsString = text as NSString
        let match = regex.firstMatch(in: text, options: [], range: NSMakeRange(0, nsString.length))
        return (match != nil)
    } catch {
        return false
    }
}



func checkCardNumber(input: String) -> (CardType) {
    // Get only numbers from the input string
    let numberOnly = input.replacingOccurrences(of: "[^0-9]", with: "")
    
    var type: CardType = .Unknown
    var formatted = ""
    var valid = false
    
    // detect card type
    for card in CardType.allCards {
        if (matchesRegex(regex: card.regex, text: numberOnly)) {
            type = card
            break
        }
    }
    
    // return the tuple
    return type
}


extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController
            
            if let top = moreNavigationController.topViewController, top.view.window != nil {
                return topViewController(base: top)
            } else if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

extension UIView{
    func setGradientBackground(colorTop: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
}

func border(view:UIView, color: UIColor, br_width: CGFloat){
    
    view.layer.borderWidth = br_width
    view.layer.borderColor = color.cgColor
}
