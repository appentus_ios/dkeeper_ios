//
//  constants.swift
//  D.KEEPER
//
//  Created by Ayush Pathak on 12/05/20.
//  Copyright © 2020 Rajat Pathak. All rights reserved.
//

import Foundation

////MARK:- Api urls
let kBaseUrl = "http://23.235.200.168/~dkeeper/Dcook/api/Api/"
let kLoginByEmail = "user_login"
let kSignupByEmail = "user_signup"
let kSocialLogin = "user_social_login"

let kGreenBaseColor = hexStringToUIColor(hex: "3CFDBE")
var fcm_token = ""
