//
//  apiFunctions.swift
//  metropolitan
//
//  Created by Love on 31/08/18.
//  Copyright © 2018 Appentus. All rights reserved.
//

import Foundation
import Alamofire

class APIFunc{
    
    class func postAPI(url: String , parameters: [String:Any] , completion: @escaping ([String:Any]) -> ()){

        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            let apiURL = url
            let param = parameters
            Alamofire.request(apiURL, method: .post, parameters: param).validate().responseString { (response) in
                switch response.result {
                case .success:
                    let responseJson = anyConvertToDictionary(text: response.result.value!)
                    completion(responseJson!)
                    break
                case .failure(let error):
                    JJHUD.hide()
                    completion(["error":true,"message":error.localizedDescription])
                    print(error)
                    
                    //                completion(["status":"false"])
                    
                    break
                }
            }
        }else{
            JJHUD.showError(text: "Internet Connection not Available!", delay: 1.0)
        }
        
        
    }
    
    class func getAPI(url: String , parameters: [String:Any] , completion: @escaping ([String:Any]) -> ()){
        
        if Reachability.isConnectedToNetwork(){
            let apiURL = url
            let param = parameters
            Alamofire.request(apiURL, method: .get, parameters: param).validate().responseString { (response) in
                switch response.result {
                case .success:
                    let responseJson = anyConvertToDictionary(text: response.result.value!)
                    completion(responseJson!)
                    break
                case .failure:
                    JJHUD.hide()
                    completion(["error":true])

                    //                completion(["status":"false"])
                    break
                }
            }
            
            
        }else{
            JJHUD.showError(text: "Internet Connection not Available!", delay: 1.0)
        }

    }
    
    
    class func postApiMultiPart(url: String ,imageParamaterName : String, parameters: [String:Any] ,imageData: Data, completion: @escaping (NSDictionary) -> ()){
        
        if Reachability.isConnectedToNetwork(){
            
            let date = NSDate()
            let df = DateFormatter()
            df.dateFormat = "dd-mm-yy-hh-mm-ss"
            
            let imageName = df.string(from: date as Date)
            
            Alamofire.upload(multipartFormData: { (multipartFormData : MultipartFormData) in
                multipartFormData.append(imageData, withName: imageParamaterName,fileName: "\(imageName)file.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }, to: url) { (result) in
                print(result)
                switch result {
                case .success(let upload, _ , _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        
                        print("uploding")
                    })
                    
                    upload.responseJSON { response in
                        
                        let resp = response.result.value! as! NSDictionary
                        completion(resp)
                        
                    }
                    
                case .failure(let encodingError):
                    print("failed")
                    completion(["error":true])

                    //                completion(["status":"false"])
                    print(encodingError)
                    
                }
            }
            
        }else{
            JJHUD.showError(text: "Internet Connection not Available!", delay: 1.0)
        }

        
  

    }
    
}







func isValidEmail(testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}
