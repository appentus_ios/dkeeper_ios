//
//  Constant.swift
//  D.KEEPER
//
//  Created by iOS-Appentus on 23/March/2020.
//  Copyright © 2020 Rajat Pathak. All rights reserved.
//

import Foundation
import UIKit


extension UITextField {
    func shake(_ colorBorder:UIColor) {
        self.layer.borderColor = colorBorder.cgColor
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.repeatCount = 4
        animation.duration = 0.04//0.5/TimeInterval(animation.repeatCount)
        animation.autoreverses = true
        animation.values = [5, -5]
        layer.add(animation, forKey: "shake")
    }
    
    func shakeError() {
        self.layer.borderColor = UIColor .red.cgColor
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.repeatCount = 4
        animation.duration = 0.04//0.5/TimeInterval(animation.repeatCount)
        animation.autoreverses = true
        animation.values = [5, -5]
        layer.add(animation, forKey: "shake")
    }
    
}

//extension String {
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
//}
