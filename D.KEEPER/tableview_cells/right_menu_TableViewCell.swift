//
//  right_menu_TableViewCell.swift
//  D.KEEPER
//
//  Created by iOS-Appentus on 20/July/2019.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class right_menu_TableViewCell: UITableViewCell {
    @IBOutlet weak var menu_title_lbl:UILabel!
    @IBOutlet weak var menu_icon_img:UIImageView!
    
    @IBOutlet weak var select_btn:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
