//
//  notifications_VC.swift
//  D.KEEPER
//
//  Created by ayush pathak on 15/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class notifications_cells: UITableViewCell {
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_desc: UILabel!
    @IBOutlet weak var img_icon: UIImageView!
    
    @IBOutlet weak var details_btn: UIButton!
    
    @IBOutlet weak var width_details: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        details_btn.layer.cornerRadius = details_btn.frame.size.height/2
        
        details_btn.layer.shadowOpacity = 1.0
        details_btn.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        details_btn.layer.shadowRadius = 6.0
        details_btn.layer.shadowColor = UIColor .lightGray.cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }


}
