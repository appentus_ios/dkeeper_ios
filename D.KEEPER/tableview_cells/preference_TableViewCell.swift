//  preference_TableViewCell.swift
//  D.KEEPER

//  Created by appentus on 7/23/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.



import UIKit



class preference_TableViewCell: UITableViewCell {
    @IBOutlet weak var view_container:UIView!
    @IBOutlet weak var title_lbl:UILabel!
    @IBOutlet weak var info_btn:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        view_container.layer.cornerRadius = 30
        view_container.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        
    }

    
    
}
