//
//  questions_cells.swift
//  D.KEEPER
//
//  Created by ayush pathak on 15/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class questions_cells: UITableViewCell {
    
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var title_lbl: UILabel!
    
    @IBOutlet weak var view_answer: UIView!
    @IBOutlet weak var answer_lbl: UILabel!
    
    @IBOutlet weak var select_btn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.layer.masksToBounds = true
        self.contentView.layer.cornerRadius = 2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
}
