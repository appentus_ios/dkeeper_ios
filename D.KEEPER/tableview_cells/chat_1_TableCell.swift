//
//  chat_1_TableCell.swift
//  D.KEEPER
//
//  Created by appentus on 7/25/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class chat_1_TableCell: UITableViewCell {

    @IBOutlet weak var chat_lbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        chat_lbl.layer.cornerRadius = 4
        chat_lbl.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
