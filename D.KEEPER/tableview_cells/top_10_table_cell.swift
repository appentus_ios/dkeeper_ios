//
//  top_10_table_cell.swift
//  D.KEEPER
//
//  Created by appentus on 7/26/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class top_10_table_cell: UITableViewCell {
    
    @IBOutlet weak var view_1: UIView!
    @IBOutlet weak var detail_btn: UIButton!



    override func awakeFromNib() {
        super.awakeFromNib()
    
        view_1.layer.cornerRadius = 4
        view_1.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
