//
//  menu_day_book_2_TableCell.swift
//  D.KEEPER
//
//  Created by appentus on 7/24/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class menu_day_book_2_TableCell: UITableViewCell {

    
    @IBOutlet weak var view_1: UIView!
    @IBOutlet weak var view_2: UIView!
    @IBOutlet weak var time_Lbl: UILabel!
    @IBOutlet weak var wake_lbl: UILabel!
    @IBOutlet weak var detail_lbl: UILabel!
    @IBOutlet weak var edit_imgView: UIButton!
    @IBOutlet weak var swipe_Img: UIButton!
    @IBOutlet weak var bar_lbl: UILabel!
    
    

    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       view_1.layer.cornerRadius = 4
        view_1.clipsToBounds = true
        view_2.layer.cornerRadius = 4
        view_2.clipsToBounds = true

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
