//
//  receipes_Cell.swift
//  D.KEEPER
//
//  Created by Rajat Pathak on 12/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class receipes_Cell: UITableViewCell {

    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var title_lbl: UILabel!
    @IBOutlet weak var buttn: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        view_back.layer.masksToBounds = true
        view_back.layer.cornerRadius = view_back.frame.size.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
