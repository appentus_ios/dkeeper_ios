//
//  personal_menu_2_TableCell.swift
//  D.KEEPER
//
//  Created by appentus on 7/24/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class personal_menu_2_TableCell: UITableViewCell {
    
    @IBOutlet weak var view_1: UIView!
    @IBOutlet weak var time_Lbl: UILabel!
    @IBOutlet weak var wake_lbl: UILabel!
    @IBOutlet weak var detail_lbl: UILabel!
    @IBOutlet weak var date_imgView: UIButton!
    @IBOutlet weak var detail_Img: UIButton!
    @IBOutlet weak var bar_lbl: UILabel!

    

    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        shadow_on_view(view: view_1, color: .lightGray, corner_radius: 4)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
