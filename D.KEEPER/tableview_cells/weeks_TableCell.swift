//
//  weeks_TableCell.swift
//  D.KEEPER
//
//  Created by appentus on 7/24/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class weeks_TableCell: UITableViewCell {

    @IBOutlet weak var view_1: UIView!
    @IBOutlet weak var weight_lbl: UILabel!
    @IBOutlet weak var week_lbl: UILabel!
    @IBOutlet weak var button: UIButton!



    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        shadow_on_view(view: view_1, color: .clear, corner_radius: view_1.frame.height/2)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
