//
//  store_delicious_CollectionViewCell.swift
//  D.KEEPER
//
//  Created by appentus on 9/12/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class store_delicious_CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var title_lbl: UILabel!
    @IBOutlet weak var image_View: UIImageView!
    @IBOutlet weak var selection_btn: UIButton!
}
