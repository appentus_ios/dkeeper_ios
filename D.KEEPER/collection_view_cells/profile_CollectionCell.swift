//
//  profile_CollectionCell.swift
//  D.KEEPER
//
//  Created by appentus on 7/18/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class profile_CollectionCell: UICollectionViewCell {
    @IBOutlet weak var cell_View: UIView!
    @IBOutlet weak var delicious_lbl: UILabel!
    @IBOutlet weak var week_lbl: UILabel!
    @IBOutlet weak var weight_this_week_lbl: UILabel!
    @IBOutlet weak var down_this_week_lbl: UILabel!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var pink_arrow_img: UIImageView!
    @IBOutlet weak var icon_img: UIImageView!
    




    
    
    
    override func awakeFromNib() {
        super .awakeFromNib()
        cell_View.layer.cornerRadius = 4.0
        cell_View.clipsToBounds = true
    }
    
}
