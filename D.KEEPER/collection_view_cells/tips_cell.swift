//
//  tips_cell.swift
//  D.KEEPER
//
//  Created by Rajat Pathak on 12/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class tips_cell: UICollectionViewCell {
    
    @IBOutlet weak var image_view: UIImageView!
    @IBOutlet weak var title_lbl: UILabel!
    @IBOutlet weak var subtitle_lbl: UILabel!
    @IBOutlet weak var btn_click: UIButton!
    
}
