//
//  walkthrough_collectionCell.swift
//  D.KEEPER
//
//  Created by ayush pathak on 16/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class walkthrough_collectionCell: UICollectionViewCell {
    @IBOutlet weak var title_up_lbl:UILabel!
    @IBOutlet weak var title_lbl:UILabel!
    @IBOutlet weak var desc_lbl:UILabel!
    
    @IBOutlet weak var img_icon:UIImageView!
    @IBOutlet weak var title_btn:UIButton!
    
    
    override func awakeFromNib() {
        title_btn.layer.cornerRadius = title_btn.bounds.height/2
        title_btn.clipsToBounds = true
    }
    
    
    
}
