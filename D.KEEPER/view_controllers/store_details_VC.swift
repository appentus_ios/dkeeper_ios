//
//  store_details_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/23/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class store_details_VC: UIViewController {
    @IBOutlet weak var plus_minus_view:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        plus_minus_view.layer.cornerRadius = 25
        plus_minus_view.clipsToBounds = true
    }
    
    @IBAction func back(_ sender: UIButton) {
        dismiss_VC()
    }
    
    @IBAction func side_menu_btn(_ sender: Any) {
    sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
}
