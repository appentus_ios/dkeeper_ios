//
//  settings_VC.swift
//  D.KEEPER
//
//  Created by Rajat Pathak on 12/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class settings_VC: UIViewController {

    var is_current_VC = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sideMenuController?.rightViewPresentationStyle = .scaleFromBig
        sideMenuController?.rightViewStatusBarStyle = .lightContent
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_push(notification:)), name: NSNotification.Name (rawValue: "push_setting"), object: nil)
    }
    
    @objc func func_push(notification:Notification) {
//        if !is_current_VC {
//            is_current_VC = !is_current_VC
            push_VC(vc_name:"\(notification.userInfo!["identifier"]!)", storybord_name: "Main")
//        }
//       
    }
    
    @IBAction func menu_btn(_ sender: Any) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
    @IBAction func back(_ sender: UIButton) {
        dismiss_VC()
    }
    @IBAction func search_Btn(_ sender: Any) {
        push_VC(vc_name: "search_VC", storybord_name: "DELICIOUS")
    }
    

}
