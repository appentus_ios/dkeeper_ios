//  dashboard_VC.swift
//  D.KEEPER
//
//  Created by Rajat Pathak on 11/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.


import UIKit
import LGSideMenuController

class dashboard_VC: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.rightViewPresentationStyle = .scaleFromBig
        sideMenuController?.rightViewStatusBarStyle = .lightContent
    }
    
    override var prefersStatusBarHidden : Bool {
        return false
    }
    
    override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        return .none
    }
    
    func initviews() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func menu_btn(_ sender: Any) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
}
