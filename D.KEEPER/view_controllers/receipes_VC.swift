//
//  receipes_VC.swift
//  D.KEEPER
//
//  Created by Rajat Pathak on 12/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class receipes_VC: UIViewController , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var table_view: UITableView!
    
    var title_array = ["חומוס ביתי","ממרח עדשים כתומים ושקדים","קציצות קישואים ברוטב אדום חריף","פלאפל אפוי","ריבועי שומשום ופצפוצי אורז","קרמבל תפוחי עץ","קרמבל תפוחי עץ","קרמבל תפוחי עץ","קרמבל תפוחי עץ","קרמבל תפוחי עץ","חומוס ביתי","ממרח עדשים כתומים ושקדים","קציצות קישואים ברוטב אדום חריף","פלאפל אפוי","ריבועי שומשום ופצפוצי אורז","קרמבל תפוחי עץ","קרמבל תפוחי עץ","קרמבל תפוחי עץ","קרמבל תפוחי עץ","קרמבל תפוחי עץ"]
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return title_array.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "receipes_Cell") as! receipes_Cell
        cell.buttn.addTarget(self, action: #selector(select_btn(_:)), for: .touchUpInside)
        
        cell.title_lbl.text = title_array[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func open_menu(_ sender: UIButton) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)

    }
    
    @IBAction func select_btn(_ sender: UIButton){
      push_VC(vc_name: "receipes_details_VC", storybord_name: "Main")
    }
    
    @IBAction func talk_btn(_ sender: UIButton){
       
        push_VC(vc_name: "questions_VC", storybord_name: "Main")

    }
}
