//
//  d_elicious_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/18/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class d_elicious_VC: UIViewController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    
    @IBAction func back(_ sender: Any) {
    }
    
}

extension d_elicious_VC: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "d_elicious_CollectionCell", for: indexPath) as! d_elicious_CollectionCell
       
        return Cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 146, height: 50)
    }
}
