//
//  profile_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/18/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class profile_VC: UIViewController , UIImagePickerControllerDelegate,UINavigationControllerDelegate{

    @IBOutlet weak var pro_1_view: UIView!
    @IBOutlet weak var pro_2_view: UIView!
    @IBOutlet weak var pro_img: UIImageView!
    @IBOutlet weak var name_Lbl: UILabel!
    @IBOutlet weak var email_Lbl: UILabel!
    
    let pickerImage =  UIImagePickerController()
    
    
    let pulsator = Pulsator()
    let kMaxRadius: CGFloat = 200
    let kMaxDuration: TimeInterval = 10
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shadow_on_view(view: pro_1_view, color: UIColor.clear, corner_radius: pro_1_view.frame.height / 2)
        shadow_on_view(view: pro_2_view, color: UIColor.clear, corner_radius: pro_2_view.frame.height / 2)
        shadow_on_view(view: pro_img, color: UIColor.clear, corner_radius: pro_img.frame.height / 2)
        
        sideMenuController?.rightViewPresentationStyle = .scaleFromBig
        sideMenuController?.rightViewStatusBarStyle = .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
         pickerImage.delegate = self
        
        pro_img.layer.superlayer?.insertSublayer(pulsator, below: pro_img.layer)
        pulsator.position = pro_img.layer.position
        
        pulsator.numPulse = 8
        pulsator.radius = 0.6 * kMaxRadius
        pulsator.animationDuration = 0.8 * kMaxDuration
        pulsator.backgroundColor = UIColor.init(red: 200.0/255.0, green: 212.0/255.0, blue: 230.0/255.0, alpha: 1.0).cgColor
        pulsator.start()
        
        
    }
    
    @IBAction func menu_btn(_ sender: Any) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
    @IBAction func edit_profile(_ sender: Any) {
        
    }
    
    @IBAction func back_btn(_ sender: Any) {
        dismiss_VC()
    }
    
    @IBAction func cell_btn(_ sender: UIButton){
        if sender.tag == 0{
            push_VC(vc_name: "delicious_VC", storybord_name: "DELICIOUS")
        }else{
            push_VC(vc_name: "d_fit_VC", storybord_name: "DELICIOUS")

        }
    }
    @IBAction func add_btn(_ sender: Any) {
       
    }
    
    @IBAction func ask_btn(_ sender: Any) {
        push_VC(vc_name: "chat_VC", storybord_name: "DELICIOUS")
    }
    
    @IBAction func info_btn(_ sender: Any) {
        push_VC(vc_name: "main_info_preferenceVC", storybord_name: "DELICIOUS")

        
    }
    
    
    @IBAction func profile_image_Edit_Btn(_ sender: Any) {
        
        pickerImage.allowsEditing = false
        pickerImage.sourceType = .photoLibrary
        
        present(pickerImage, animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            pro_img.contentMode = .scaleAspectFill
            pro_img.image = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
}


extension profile_VC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "profile_CollectionCell", for: indexPath) as! profile_CollectionCell
        if indexPath.row == 0{
            Cell.icon_img.image = UIImage(named: "Group 3360")
            Cell.cell_View.backgroundColor = hexStringToUIColor(hex: "#EB92A1")
            Cell.delicious_lbl.text = "D.ELICIOUS"
            Cell.pink_arrow_img.image = UIImage(named: "pink_arrow")
           
        } else {
            Cell.icon_img.image = UIImage(named: "dfit_icon_black")
            Cell.cell_View.backgroundColor = hexStringToUIColor(hex: "#294360")
            Cell.delicious_lbl.text = "D.FIT"
            Cell.pink_arrow_img.image = UIImage(named: "blue_arrow")
            
        }
        
        Cell.btn.tag = indexPath.row
        Cell.btn.addTarget(self, action: #selector(cell_btn(_:)), for: .touchUpInside)
        return Cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 220, height: 300)
    }
    
}

