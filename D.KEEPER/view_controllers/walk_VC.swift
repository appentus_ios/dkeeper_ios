//
//  walk_VC.swift
//  D.KEEPER
//
//  Created by appentus on 8/9/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class walk_VC: UIViewController {
    
    
    @IBOutlet weak var menu_view: MenuTabsView!
    
    var once = false
    var curr = Int()
    var currentIndex: Int = 0
    var tabs = ["D.FIT", "D.ELICIOUS","D.ELICIOUS + D.FIT", "D.ELICIOUS + D.FIT", "D.COOK"]
    var pageController: UIPageViewController!
    
    var arrVC = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        curr = 0
        let controller_1  = storyboard?.instantiateViewController(withIdentifier: "d_fit_walkVC") as! d_fit_walkVC
        let controller_2  = storyboard?.instantiateViewController(withIdentifier: "delicious_walkVC") as! delicious_walkVC
        let controller_3 =  storyboard?.instantiateViewController(withIdentifier: "delicious_dfit_walkVC") as! delicious_dfit_walkVC
        let controller_4  = storyboard?.instantiateViewController(withIdentifier: "delicious_dfit_walkVC_2") as! delicious_dfit_walkVC_2
        let controller_5 = storyboard?.instantiateViewController(withIdentifier: "dcook_walkVC") as! dcook_walkVC
        
        arrVC = [controller_1,controller_2,controller_3,controller_4,controller_5]
        
        menu_view.dataArray = tabs
        menu_view.isSizeToFitCellsNeeded = true
        menu_view.collView.backgroundColor = .clear
        presentPageVCOnView()
        
        menu_view.menuDelegate = self
        pageController.delegate = self
        pageController.dataSource = self
        
        
        //For Intial Display
        pageController.setViewControllers([arrVC[0]], direction: .forward, animated: true, completion: nil)
        pageController.view.semanticContentAttribute = .forceRightToLeft
        
        let current_index = 0
        
        for i in 0..<color_arr.count{
            if i == current_index{
                color_arr[i] = "#3CFDBE"
            }else{
                color_arr[i] = "#FFFFFF"
            }
        }
        
        for i in 0..<color_arr_2.count{
            if i == current_index{
                arrrr[i] =  hexStringToUIColor(hex: "#3CFDBE")
                //                        "#3CFDBE"
            }else{
                arrrr[i] = UIColor.clear
            }
        }
        
        
        
        menu_view.collView.reloadData()
        menu_view.collView.selectItem(at: IndexPath.init(item: current_index, section: 0), animated: true, scrollPosition: .centeredVertically)
        menu_view.collView.scrollToItem(at: IndexPath.init(item: current_index, section: 0), at: .centeredHorizontally, animated: true)

    }
    
    func presentPageVCOnView() {
        
        self.pageController = storyboard?.instantiateViewController(withIdentifier: "walk_pageVC") as! walk_pageVC
        self.pageController.view.frame = CGRect.init(x: 0, y: menu_view.frame.maxY, width: view.frame.width, height: view.frame.height - menu_view.frame.maxY)
        self.addChild(self.pageController)
        self.view.addSubview(self.pageController.view)
        self.pageController.didMove(toParent: self)
//        pageController.
        
    }
    
    func viewController(At index: Int) -> UIViewController? {
        
        if((self.menu_view.dataArray.count == 1) || (index >= self.menu_view.dataArray.count)) {
            return nil
        } else {
            let vc = self.arrVC[index]
            currentIndex = index
            return vc
        }
    }
    
    @IBAction func menu_btn(_ sender: Any) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
    @IBAction func back(_ sender: UIButton){
        self.dismiss_VC()
    }

}


extension walk_VC: MenuBarDelegate {
    
    func menuBarDidSelectItemAt(menu: MenuTabsView, index: Int) {
        
        // If selected Index is other than Selected one, by comparing with current index, page controller goes either forward or backward.
        
        if index != currentIndex {
            
            if index > currentIndex {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .forward, animated: true, completion: nil)
            }else {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .reverse, animated: true, completion: nil)
            }
            for i in 0..<color_arr.count{
                if i == index{
                    color_arr[i] = "#3CFDBE"
                }else{
                    color_arr[i] = "#FFFFFF"
                }
            }
            
            for i in 0..<color_arr_2.count{
                if i == index{
                    arrrr[i] =  hexStringToUIColor(hex: "#3CFDBE")
                    //                        "#3CFDBE"
                }else{
                    arrrr[i] = UIColor.clear
                }
            }
            menu_view.collView.reloadData()
            menu_view.collView.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .centeredHorizontally, animated: true)
            
        }
        
    }
    
}


extension walk_VC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController)-> UIViewController? {
        curr = arrVC.firstIndex(of: viewController)!
        // if you prefer to NOT scroll circularly, simply add here:
        if curr == 0 { return nil }
        let prev = abs((curr - 1) % arrVC.count)
        
        return arrVC[prev]
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController)-> UIViewController? {
        curr = arrVC.firstIndex(of: viewController)!
        // if you prefer to NOT scroll circularly, simply add here:
        if curr == (arrVC.count - 1) { return nil }
        let nxt = abs((curr + 1) % arrVC.count)
        return arrVC[nxt]
    }
    
    func nextPageWithIndex(index: Int)
    {
        // let nextWalkthroughVC = newColoredViewController("setup4")
        let nextWalkthroughVC = arrVC[index]
        self.pageController.setViewControllers([nextWalkthroughVC], direction: .forward, animated: true, completion: nil)
        
    }
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if finished {
            if completed {
                
                
                let current_index = arrVC.firstIndex(of: pageViewController.viewControllers!.first!)
                
                for i in 0..<color_arr.count{
                    if i == current_index!{
                        color_arr[i] = "#3CFDBE"
                    }else{
                        color_arr[i] = "#FFFFFF"
                    }
                }
                
                for i in 0..<color_arr_2.count{
                    if i == current_index!{
                        arrrr[i] =  hexStringToUIColor(hex: "#3CFDBE")
//                        "#3CFDBE"
                    }else{
                        arrrr[i] = UIColor.clear
                    }
                }
                
                
                
                menu_view.collView.reloadData()
                menu_view.collView.selectItem(at: IndexPath.init(item: current_index!, section: 0), animated: true, scrollPosition: .centeredVertically)
                menu_view.collView.scrollToItem(at: IndexPath.init(item: current_index!, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
    }
}


