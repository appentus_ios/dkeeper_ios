//  popup_submit_contact_VC.swift
//  D.KEEPER

//  Created by Rajat Pathak on 12/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.


import UIKit


class popup_submit_contact_VC: UIViewController {

    @IBOutlet weak var popup_view: UIView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initviews()
    }
    
    func initviews() {
        self.popup_view.layer.masksToBounds = true
        self.popup_view.layer.cornerRadius = 8.0
        self.popup_view.layer.shadowColor = UIColor.darkGray.cgColor
        self.popup_view.layer.shadowOpacity = 0.5
        self.popup_view.layer.shadowOffset = CGSize.zero
        self.popup_view.layer.shadowRadius = 5
    }
    
    @IBAction func left_btn_action(_ sender: UIButton) {
        dismissPopUpViewController()
        
    }

    @IBAction func right_btn_action(_ sender: UIButton) {
         dismissPopUpViewController()
        
    }

    
    deinit {
        print("deinit")
    }
    
}
