//  receipes_details_VC.swift
//  D.KEEPER

//  Created by ayush pathak on 15/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.


import UIKit

class receipes_details_VC: UIViewController {
    @IBOutlet weak var table_view: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
        
    @IBAction func back(_ sender: UIButton) {
        dismiss_VC()
    }

    
    @IBAction func sidemenu_btn(_ sender: Any) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)

    }
    
}
