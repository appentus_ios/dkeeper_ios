//
//  d_cook_1_vc.swift
//  D.KEEPER
//
//  Created by appentus on 7/19/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class d_cook_1_vc: UIViewController {

    
    var delegte_call: navigate_delegate?
    
    @IBOutlet weak var menu_type_TF: UITextField!
    @IBOutlet weak var mygoal_TF: UITextField!
    @IBOutlet weak var dnot_want_TF: UITextField!
    
    @IBOutlet weak var first_view: UIView!
    @IBOutlet weak var second_view: UIView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        border(view: first_view, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: first_view, color: .clear, corner_radius: first_view.frame.height/2)
        border(view: second_view, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: second_view, color: .clear, corner_radius: second_view.frame.height/2)
        border(view: menu_type_TF, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: menu_type_TF, color: .clear, corner_radius: menu_type_TF.frame.height/2)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back(_ sender: Any) {
    }
    
    @IBAction func next_level(_ sender: UIButton) {
        delegte_call?.nav_VC(tag: sender.tag)
        
    }
    
}
