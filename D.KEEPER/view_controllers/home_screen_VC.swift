//  home_screen_VC.swift
//  D.KEEPER

//  Created by appentus on 7/19/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.


import UIKit

class home_screen_VC: UIViewController {

    var once = UserDefaults.standard.object(forKey: "onecnv") as? Bool
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideMenuController?.rightViewPresentationStyle = .scaleFromBig
        sideMenuController?.rightViewStatusBarStyle = .lightContent
        
        
        if once == nil{
             once = true
        }
        if once!{
            UserDefaults.standard.set(false, forKey: "onecnv")
            once = false
            NotificationCenter.default.addObserver(self, selector: #selector(btn_right_menu(_:)), name: NSNotification.Name("nav_main"), object: nil)
        }
    }
    
    @objc func btn_right_menu(_ notification:Notification) {
        let tag = notification.userInfo!["sender"] as! Int
        if tag == 0 {
            push_VC(vc_name: "home_screen_VC", storybord_name: "Main")
        } else if tag == 1 {
            push_VC(vc_name: "notifications_VC", storybord_name: "Main")
        }else if tag == 2 {
             push_VC(vc_name: "profile_VC", storybord_name: "Main")
        }else if tag == 3 {
            push_VC(vc_name: "search_VC", storybord_name: "DELICIOUS")
        }else if tag == 4 {
              push_VC(vc_name: "dishes_preference_VC", storybord_name: "Main")
        } else if tag == 5{
           push_VC(vc_name: "receipes_details_VC", storybord_name: "Main")
        } else if tag == 6 {
            push_VC(vc_name: "questions_VC", storybord_name: "Main")
        } else if tag == 7 {
            push_VC(vc_name: "chat_VC" , storybord_name: "DELICIOUS")
        } else if tag == 8 {
             push_VC(vc_name: "store__VC", storybord_name: "Main")
        } else if tag == 9 {
             push_VC(vc_name: "tips_VC", storybord_name: "Main")
        } else if tag == 10 {
             push_VC(vc_name: "settings_VC", storybord_name: "Main")
        } else if tag == 11 {
             push_VC(vc_name: "contact_VC", storybord_name: "Main")
        }
            
    }
    
    func initviews() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func d_cook_btn(_ sender: Any) {
        push_VC(vc_name: "dishes_preference_VC", storybord_name: "Main")
    }
    
    @IBAction func d_keeper_btn(_ sender: Any) {
       push_VC(vc_name: "profile_VC", storybord_name: "Main")
    }
    
    @IBAction func d_rest_btn(_ sender: Any) {
        push_VC(vc_name: "top_10_VC", storybord_name: "DELICIOUS")
    }
    
    @IBAction func d_tips_btn(_ sender: Any) {
        push_VC(vc_name: "tips_VC", storybord_name: "Main")
    }
    
    @IBAction func menu_btn(_ sender: Any) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
}
