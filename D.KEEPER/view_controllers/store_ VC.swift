//  store_ VC.swift
//  D.KEEPER

//  Created by ayush pathak on 15/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.


import UIKit

class store__VC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
        
    @IBAction func back(_ sender: UIButton) {
       dismiss_VC()
    }
    
    @IBAction func menu_btn(_ sender: UIButton) {
       sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
}



//  MARK:- UICollectionView methods
extension store__VC:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width/2-30
        return CGSize (width: width, height: 214)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "store_cell", for: indexPath) as! store_CollectionCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        push_VC(vc_name: "store_details_VC", storybord_name: "Main")
    }
    
    
}


