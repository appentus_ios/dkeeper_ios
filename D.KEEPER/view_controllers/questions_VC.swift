//
//  questions_VC.swift
//  D.KEEPER
//
//  Created by ayush pathak on 15/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class questions_VC: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var table_view: UITableView!
    
    var title_array = ["במה שונה די.קיפר מכל מה שהכרתי עד היום, ולמה שהפעם אצליח? י","איך מתבצע מעקב השקילה אם לא נפגשים פרונטלית? י","לא מפסיקים עם פחמימות נכון? י","אפשר להיפטר מהחשק למתוק? י","במה שונה די.קיפר מכל מה שהכרתי עד היום, ולמה שהפעם אצליח? י","איך מתבצע מעקב השקילה אם לא נפגשים פרונטלית? י","לא מפסיקים עם פחמימות נכון? י","אפשר להיפטר מהחשק למתוק? י"]
        
//    var title_array = ["ריבועי שומשום ופצפוצי אורז","קרמבל תפוחי עץ","קרמבל תפוחי עץ","קרמבל תפוחי עץ","קרמבל תפוחי עץ","קרמבל תפוחי עץ","חומוס ביתי","ממרח עדשים כתומים ושקדים","קציצות קישואים ברוטב אדום חריף","פלאפל אפוי","ריבועי שומשום ופצפוצי אורז","קרמבל תפוחי עץ","קרמבל תפוחי עץ","קרמבל תפוחי עץ","קרמבל תפוחי עץ","קרמבל תפוחי עץ"]
    
    var arr_select = [Bool]()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for _ in title_array {
            arr_select.append(false)
        }
        sideMenuController?.rightViewPresentationStyle = .scaleFromBig
        sideMenuController?.rightViewStatusBarStyle = .lightContent
    }
    
    @IBAction func menu_btn(_ sender: UIButton){
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return title_array.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "receipes_Cell") as! questions_cells
        
        cell.title_lbl.text = title_array[indexPath.row]
        
        if arr_select[indexPath.row] {
            cell.view_answer.isHidden = false
        } else {
            cell.view_answer.isHidden = true
        }
        
        cell.select_btn.tag = indexPath.row
        cell.select_btn.addTarget(self, action: #selector(select_btn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arr_select[indexPath.row] {
            return 284
        } else {
            return 104
        }
    }
    
    @IBAction func select_btn(_ sender: UIButton) {
        for i in 0..<arr_select.count {
            if i == sender.tag {
                if arr_select[i] {
                   arr_select[i] = false
                } else {
                   arr_select[i] = true
                }
            }
        }
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        table_view.reloadRows(at: [indexPath], with: .automatic)
        table_view.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    @IBAction func back(_ sender: UIButton) {
        dismiss_VC()
    }
    
}
