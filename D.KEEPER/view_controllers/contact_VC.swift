//
//  contact_VC.swift
//  D.KEEPER
//
//  Created by Rajat Pathak on 12/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class contact_VC: UIViewController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

 
    @IBAction func send_message_to_contact(_ sender: UIButton) {
        self.viewPresent(storyBoard: "Main", viewId: "contact_form_VC")
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func side_right_action(_ sender: UIButton) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
}
