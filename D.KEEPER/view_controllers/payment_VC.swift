//
//  payment_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/19/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class payment_VC: UIViewController , UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{

    @IBOutlet weak var view_1: UIView!
    @IBOutlet weak var view_2: UIView!
   
    @IBOutlet weak var view_3: UIView!
    
    @IBOutlet weak var view_4: UIView!
    
    @IBOutlet weak var view_: UIView!
    
    
    @IBOutlet weak var card_num_TF: UITextField!
    @IBOutlet weak var card_validity_TF: UITextField!
    @IBOutlet weak var id_TF: UITextField!
    @IBOutlet weak var verification_code_TF: UITextField!
    @IBOutlet weak var card_code_TF: UITextField!
    
    var delegte_call: navigate_delegate?
    var myPickerView : UIPickerView!
    var pickerData = ["Female" , "Male"]

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
          pickUp(card_code_TF)
        
        border(view: view_1, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_1, color: .clear, corner_radius: view_1.frame.height/2)
        
        border(view: view_2, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_2, color: .clear, corner_radius: view_2.frame.height/2)
        
        border(view: view_3, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_3, color: .clear, corner_radius: view_3.frame.height/2)
        
        border(view: view_4, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_4, color: .clear, corner_radius: view_4.frame.height/2)
        
        border(view: view_, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_, color: .clear, corner_radius: view_.frame.height/2)

        // Do any additional setup after loading the view.
    }
    

    @IBAction func back(_ sender: Any) {
    }
    
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        textField.inputView = self.myPickerView
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if card_code_TF.isFirstResponder == true {
            self.card_code_TF.text = pickerData[row]
            
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return pickerData[row]
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if card_code_TF.isFirstResponder == true {
            self.pickUp(card_code_TF)
        }
        
    }
    
    
    @IBAction func nextLvl_Btn(_ sender: UIButton) {
    delegte_call?.nav_VC(tag: sender.tag)
    }
    
}
