//
//  register_VC.swift
//  D.KEEPER
//
//  Created by Rajat Pathak on 11/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class register_VC: UIViewController {

    @IBOutlet weak var name_TF: UITextField!
    @IBOutlet weak var phone_TF: UITextField!
    @IBOutlet weak var checkBox: UIButton!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initviews()
        // Do any additional setup after loading the view.
    }
    
    func initviews() {
        name_TF.layer.masksToBounds = true
        phone_TF.layer.masksToBounds = true
        name_TF.layer.cornerRadius = name_TF.frame.height / 2
        phone_TF.layer.cornerRadius = phone_TF.frame.height / 2
    }
    
    @IBAction func register_btn(_ sender: UIButton) {
        signupHandler()
        
    }
    
    @IBAction func login_btn(_ sender: UIButton) {
        dismiss_VC()
    }
    @IBAction func checkboxSelBtn(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func forgot_Pass_Btn(_ sender: Any) {
        
    }
}
extension register_VC{
    func signupHandler() {
        self.view.endEditing(true)
        if name_TF.text!.trimmingCharacters(in: .whitespaces).count == 0{
            self.view.showError("Enter valid name")
            return
        }
        
        if self.phone_TF.text!.trimmingCharacters(in: .whitespaces).count == 0{
            self.view.showError("Enter valid number")
           return
        }
        
        if self.checkBox.isSelected == false{
            self.view.showError("Agree to terms and conditions to proceed")
            return 
        }
        
        signupModel.shared.name = name_TF.text!
        signupModel.shared.phoneNumber = phone_TF.text!
        self.viewPresent(storyBoard: "Main", viewId: "register2_VC")
    }
}
