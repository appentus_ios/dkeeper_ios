//
//  signup_select.swift
//  D.KEEPER
//
//  Created by Rajat Pathak on 11/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import InstagramLogin

class signup_select: UIViewController {
    
    var instagramLogin: InstagramLoginViewController!
    let clientId = "<YOUR CLIENT ID GOES HERE>"
    let redirectUri = "<YOUR REDIRECT URI GOES HERE>"
    
    //    MARK:- Outlets
    @IBOutlet weak var email_View: UIView!
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(nil, forKey: "onecnv")
        
        initViews()
    }
    
    func initViews() {
        self.email_View.layer.masksToBounds = true
        self.email_View.layer.cornerRadius = self.email_View.frame.size.height/2
        self.email_View.layer.borderColor = UIColor.init(red: 236.0/255, green: 236.0/255, blue:  236.0/255, alpha: 0.1).cgColor
        self.email_View.layer.borderWidth = 1.0
    }
    
    @IBAction func signup_email_btn(_ sender: UIButton) {
        self.viewPresent(storyBoard: "Main", viewId: "register_VC")
    }
    
    @IBAction func login_email_btn(_ sender: UIButton) {
        self.viewPresent(storyBoard: "Main", viewId: "login_VC")
    }
    
    @IBAction func facebook_sign_in_btn(_ sender: UIButton) {
        let fbmanager = LoginManager()
        fbmanager.logIn(permissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                    fbmanager.logOut()
                }
            }
        }
    }
    
    @IBAction func instagrams_sign_in_btn(_ sender: UIButton) {
        loginWithInstagram()
    }
    
    func loginWithInstagram() {
        
        instagramLogin = InstagramLoginViewController(clientId: clientId, redirectUri: redirectUri)
        instagramLogin.delegate = self
        
        instagramLogin.scopes = [.basic, .publicContent]
        instagramLogin.title = "Instagram"
        instagramLogin.progressViewTintColor = .blue
        
        instagramLogin.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissLoginViewController))
        
        instagramLogin.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshPage))
        
        present(UINavigationController(rootViewController: instagramLogin), animated: true)
    }
    @objc func dismissLoginViewController() {
        instagramLogin.dismiss(animated: true)
    }
    
    @objc func refreshPage() {
        instagramLogin.reloadPage()
    }
    
    func getFBUserData(){
        self.view.showLoader()
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil) {
                    //everything works print the user data
                    print(result!)
                    let resultJson : NSDictionary = result as! NSDictionary
                    let socialID = "\(resultJson["id"]!)"
                    let email = "\(resultJson["email"] ?? "")"
                    let name = "\(resultJson["name"]!)"
                    let imageDict : NSDictionary = resultJson["picture"] as! NSDictionary
                    let dataOne : NSDictionary = imageDict["data"] as! NSDictionary
                    let imageUrl = "\(dataOne["url"] ?? "")"
                    self.view.hideLoader()
                    DispatchQueue.main.async {
                        self.loginBySocial(true, name, email, socialID, imageUrl)
                    }
                } else {
                    self.view.hideLoader()
                }
            })
        }
    }
    func loginBySocial(_ isFbLogin:Bool,_ name:String,_ emai:String,_ socialId:String,_ imag:String) {
        if isFbLogin{
            self.view.show_loader()
            let dict = [
                "user_name":name,
                "user_email":emai,
                "user_device_type":"2",
                "user_device_token":fcm_token,
                "social_id":socialId,
            ]
            APIFunc.postAPI(url: kBaseUrl+kSocialLogin, parameters: dict) { (resp) in
                self.view.hideLoader()
                if let error = resp["error"] as? Bool{
                    print(error)
                    self.view.showError(resp["message"] as! String)
                }else{
                    let resultDict = resp
                    let status = resultDict["status"] as! String
                    if status == "success"{
                        self.view.showSuccess(resultDict["message"] as! String)
                        let dict = resultDict["result"] as! [String:Any]
                        let userId = "\(dict["user_id"]!)"
                        UserDefaults.standard.set(userId, forKey: "userId")
                        UserDefaults.standard.set(dict, forKey: "loginData")
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1.0) {
                            self.viewPresent(storyBoard: "Main", viewId: "LGSideMenuController")
                        }
                    }else{
                        self.view.showError(resultDict["message"] as! String)
                    }
                }
            }
        }else{
            
        }
    }
}

extension signup_select: InstagramLoginViewControllerDelegate {
    
    func instagramLoginDidFinish(accessToken: String?, error: InstagramError?) {
        if error == nil{
            instagramLogin.dismiss(animated: true) {
                self.getInstaProfile(accesstoken: accessToken!)
            }
        }else{
            instagramLogin.dismiss(animated: true) {
                self.view.showError(error!.localizedDescription)
            }
        }
    }
    func getInstaProfile(accesstoken :String) {
        self.view.showLoader()
        let url = "https://api.instagram.com/v1/users/self/?access_token="+accesstoken
        APIFunc.getAPI(url: url, parameters: ["":""]) { (resp) in
            self.view.hideLoader()
            
        }
    }
}
