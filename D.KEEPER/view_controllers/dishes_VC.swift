//
//  preferences_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/23/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit
import BubbleTransition

class dishes_VC: UIViewController {
    
    @IBOutlet weak var cart_BTN: UIButton!

    let transition = BubbleTransition()
    let interactiveTransition = BubbleInteractiveTransition()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  let controller = segue.destination as? d_cook_popup_VC {
        controller.transitioningDelegate = self
        controller.modalPresentationStyle = .custom
        controller.interactiveTransition = interactiveTransition
            interactiveTransition.attach(to: controller)
        }
    }
    
     override func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.startingPoint = cart_BTN.center
        transition.bubbleColor = UIColor.init(red: 25.0/255.0, green: 53.0/255.0, blue: 85.0/255.0, alpha: 0.75)

        return transition
    }
    
    override func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .dismiss
        transition.startingPoint = cart_BTN.center
        transition.bubbleColor =  UIColor.init(red: 25.0/255.0, green: 53.0/255.0, blue: 85.0/255.0, alpha: 0.75)
        return transition
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactiveTransition
    }
    
    
//
//    @IBAction func cart_Btn(_ sender: Any){
//        let modalViewController = self.storyboard?.instantiateViewController(withIdentifier: "d_cook_popup_VC") as! d_cook_popup_VC
//        modalViewController.modalPresentationStyle = .overFullScreen
//        present(modalViewController, animated: true, completion: nil)
//    }
//
    

}


extension dishes_VC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell-dishes", for: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
}
