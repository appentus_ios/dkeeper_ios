//
//  register2_VC.swift
//  D.KEEPER
//
//  Created by appentus on 9/6/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class register2_VC: UIViewController {
    
    @IBOutlet weak var email_TF: UITextField!
    @IBOutlet weak var password_TF: UITextField!
    @IBOutlet weak var confirm_pass_TF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initviews()
    }
    
    func initviews() {
        email_TF.layer.masksToBounds = true
        password_TF.layer.masksToBounds = true
        confirm_pass_TF.layer.masksToBounds = true
        
        email_TF.layer.cornerRadius = email_TF.frame.height / 2
        password_TF.layer.cornerRadius = password_TF.frame.height / 2
        confirm_pass_TF.layer.cornerRadius = confirm_pass_TF.frame.height / 2
        
        email_TF.layer.borderWidth = 1.0
        password_TF.layer.borderWidth = 1.0
        confirm_pass_TF.layer.borderWidth = 1.0
        
        email_TF.layer.borderColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.54).cgColor
        password_TF.layer.borderColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.54).cgColor
        confirm_pass_TF.layer.borderColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.54).cgColor
    }
    
    @IBAction func register(_ sender: UIButton) {
        registerHandler()
    }
    
    func registerHandler() {
        self.view.endEditing(true)
        if !isValidEmail(email: self.email_TF.text!){
            self.view.showError("Enter valid email address")
            return
        }
        
        if self.password_TF.text!.count < 6{
            self.view.showError("Password must be greater than 6 characters")
           return
        }
        if self.password_TF.text!.count != self.confirm_pass_TF.text!.count{
            self.view.showError("Password and confirm password do not matcg")
           return
        }
        signupModel.shared.email = self.email_TF.text!
        signupModel.shared.password = self.password_TF.text!
        signupUser()
    }
    
    func signupUser() {
        self.view.show_loader()
        let dict = [
            "user_name":signupModel.shared.name,
            "user_email":signupModel.shared.email,
            "user_password":signupModel.shared.password,
            "user_mobile":signupModel.shared.phoneNumber,
            "user_device_type":"2",
            "user_device_token":fcm_token,
        ]
        APIFunc.postAPI(url: kBaseUrl+kSignupByEmail, parameters: dict) { (resp) in
            self.view.hideLoader()
            if let error = resp["error"] as? Bool{
                print(error)
                self.view.showError(resp["message"] as! String)
            }else{
                let resultDict = resp
                let status = resultDict["status"] as! String
                if status == "success"{
                    self.view.showSuccess(resultDict["message"] as! String)
                    let dict = resultDict["result"] as! [String:Any]
                    let userId = "\(dict["user_id"]!)"
                    UserDefaults.standard.set(userId, forKey: "userId")
                    UserDefaults.standard.set(dict, forKey: "loginData")
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1.0) {
                        self.viewPresent(storyBoard: "Main", viewId: "LGSideMenuController")
                    }
                }else{
                    self.view.showError(resultDict["message"] as! String)
                }
            }
        }
    }
}
