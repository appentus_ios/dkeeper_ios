//
//  walkthrough_VC.swift
//  D.KEEPER
//
//  Created by ayush pathak on 16/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit
import AnimatedCollectionViewLayout

class walkthrough_VC: UIViewController {
   var arr_title_up = ["D.FIT","D.ELICIOUS","D.COOK","D.ELICIOUS + D.FIT","D.ELICIOUS + D.FIT","D.COUPLES"]
    var arr_img_icon = ["graphics_2","graphics_3","graphics_6","graphics_4","graphics_5","graphics_7"]
    var arr_title = ["תוכנית אימונים",
                     "תוכנית תזונה",
"תוכנית בישול",
"תוכנית מלאה",
"תוכנית מלאה",
"תוכנית מלאה"
]
    
    var title_arr = ["D.FIT",
                     "D.ELICIOUS",
                     "D.COOK",
                     "D.ELICIOUS + D.FIT",
                     "D.ELICIOUS + D.FIT",
                     "D.COUPLES"
    ]
    
    var arr_desc = [""]
    var arr_title_btn = ["D.FIT לרכישת תוכנית"
    , "D.ELICIOUS לרכישת תוכנית"
    , "D.COOK לרכישת תוכנית"
    , "D.KEEPER לרכישת תוכנית"
    , "D.KEEPER לרכישת תוכנית",
     "D.KEEPER לרכישת תוכנית"]
    
    
    var detailed_arr = [
  "ערכת אימונים הכוללת מדריך אימונים של 12 שבועות. בערכה תוכלו למצוא מעל 36 אימונים שונים שיסייעו לכם בחיטוב הגוף, בשריפת שומן ובשיפור סיבולת לב ריאה. תוכלו להתאמן בכל מקום, בכל זמן וללא כל צורך בחדר כושר. י",
        "ערכת תזונה הכוללת מדריך מקיף לירידה במשקל ושמירה על אורח חיים בריא. בערכה תוכלו למצוא הנחיות, טיפים, תפריטים ומתכונים ובאופן כללי- את כל הידע הדרוש לכם כדי ללמוד לבחור נכון להזין את הגוף שלכם במזונות הראויים לו. י",
        " למה לא גם וגם? רוצים ללמוד לאכול נכון, לרדת במשקל ותוך כדי להתחיל להתאמן סוף סוף? קבלו את הערכה המשולבת הכוללת גם את מדריך האימונים וגם את מדריך התזונה- והכל? בשביל להשיג מקסימום תוצאות במינימום זמן.י",
              "למה לא גם וגם? רוצים ללמוד לאכול נכון, לרדת במשקל ותוך כדי להתחיל להתאמן סוף סוף? קבלו את הערכה המשולבת הכוללת גם את מדריך האימונים וגם את מדריך התזונה- והכל? בשביל להשיג מקסימום תוצאות במינימום זמן.י",
              "למה לא גם וגם? רוצים ללמוד לאכול נכון, לרדת במשקל ותוך כדי להתחיל להתאמן סוף סוף? קבלו את הערכה המשולבת הכוללת גם את מדריך האימונים וגם את מדריך התזונה- והכל? בשביל להשיג מקסימום תוצאות במינימום זמן.י",
              "למה לא גם וגם? רוצים ללמוד לאכול נכון, לרדת במשקל ותוך כדי להתחיל להתאמן סוף סוף? קבלו את הערכה המשולבת הכוללת גם את מדריך האימונים וגם את מדריך התזונה- והכל? בשביל להשיג מקסימום תוצאות במינימום זמן.י"
              ]
    
    @IBOutlet weak var page_controller:UIPageControl!
    @IBOutlet weak var collection_v: UICollectionView!
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        page_controller.numberOfPages = arr_img_icon.count
        if let layout = collection_v?.collectionViewLayout as? AnimatedCollectionViewLayout {
            layout.animator = ZoomInOutAttributesAnimator()
//            layout.animator = PageAttributesAnimator()
        }
    }
}



//  MARK:- UICollectionView methods
extension walkthrough_VC:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = collectionView.bounds
        return CGSize (width:collection_v.frame.width, height: collection_v.frame.height - 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr_title_up.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "walkthrough_collectionCell", for: indexPath) as! walkthrough_collectionCell
        
        cell.title_up_lbl.text = title_arr[indexPath.row]
        cell.title_lbl.text = arr_title[indexPath.row]
        
        cell.img_icon.image = UIImage (named:arr_img_icon[indexPath.row])
        cell.title_btn.setTitle(arr_title_btn[indexPath.row], for: .normal)
        cell.title_btn.tag = indexPath.row
        cell.title_btn.addTarget(self, action:#selector(title_btn(_:)), for: .touchUpInside)
        cell.desc_lbl.text = detailed_arr[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        page_controller.currentPage = Int(pageNumber)
    }
    
    @IBAction func title_btn(_ sender:UIButton) {
//        let visibleItems: NSArray = self.collection_v.indexPathsForVisibleItems as NSArray
//        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
//        let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
//        if nextItem.row < arr_title_up.count {
//            self.collection_v.scrollToItem(at: nextItem, at: .left, animated: true)
//        } else {
//            self.viewPresent(storyBoard: "Main", viewId: "signup_select")
//        }
    
//        self.viewPresent(storyBoard: "Main", viewId: "LGSideMenuController")
        
        let storyboard = UIStoryboard(name: "DELICIOUS", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "store_selection_VC") as! store_selection_VC
        vc.image_selected = UIImage(named: arr_img_icon[sender.tag])
        vc.title_selected = title_arr[sender.tag]
        vc.selected_inde = sender.tag
        vc.header_selected = arr_title_up[sender.tag]
        vc.detailed_selected = detailed_arr[sender.tag]
        vc.selected_inde = sender.tag
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
    
    
    
    
    @IBAction func skip(_ sender:UIButton) {
        self.viewPresent(storyBoard: "Main", viewId: "signup_select")
//        push_VC(vc_name: "LGSideMenuController", storybord_name: "Main")
    }
    
    
    
}


