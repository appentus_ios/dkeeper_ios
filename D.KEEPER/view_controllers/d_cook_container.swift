
//  d_cook_container.swift
//  D.KEEPER

//  Created by ayush pathak on 20/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.


import UIKit
import Motion

class d_cook_container: UIViewController, navigate_delegate {
  
    
    @IBOutlet weak var menu_view: MenuTabsView!
    
    var once = false
    var curr = Int()
    var currentIndex: Int = 0
    var tabs = ["מסלולים"
    , "העדפות"
    , "הצהרת בריאות"
    , "פרטי חיוב"
    , "שיטת תשלום"
    , "אישור הזמנה"
    , "סנכרון לתפריט"
    , "העדפות"
    , "מנות"
    ]
    var pageController: UIPageViewController!
    
    var arrVC = [UIViewController]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        curr = 0
        let controller_1  = storyboard?.instantiateViewController(withIdentifier: "d_cook_VC") as! d_cook_VC
        let controller_2  = storyboard?.instantiateViewController(withIdentifier: "d_cook_1_vc") as! d_cook_1_vc
        let controller_3 =  storyboard?.instantiateViewController(withIdentifier: "purchase_VC") as! purchase_VC
        let controller_4  = storyboard?.instantiateViewController(withIdentifier: "billing_VC") as! billing_VC
        let controller_5 = storyboard?.instantiateViewController(withIdentifier: "payment_VC") as! payment_VC
        let controller_6 = storyboard?.instantiateViewController(withIdentifier: "order_confirm_VC") as! order_confirm_VC
        let controller_7 = storyboard?.instantiateViewController(withIdentifier: "sync_VC") as! sync_VC
        let controller_8 = storyboard?.instantiateViewController(withIdentifier: "preferences_VC") as! preferences_VC
        let controller_9 = storyboard?.instantiateViewController(withIdentifier: "dishes_VC") as! dishes_VC

        controller_2.delegte_call = self
        controller_1.delegte_call = self
        controller_3.delegte_call = self
        controller_4.delegte_call = self
        controller_5.delegte_call = self
        controller_6.delegte_call = self
        
        
        
        arrVC = [controller_1,controller_2,controller_3,controller_4,controller_5,controller_6,controller_7,controller_8,controller_9]
        menu_view.dataArray = tabs
        menu_view.isSizeToFitCellsNeeded = true
        menu_view.collView.backgroundColor = .clear
        presentPageVCOnView()
        
        menu_view.menuDelegate = self
        pageController.delegate = self
        pageController.dataSource = self
        
        //For Intial Display
        menu_view.collView.selectItem(at: IndexPath.init(item: 0, section: 0), animated: true, scrollPosition: .right)
        pageController.setViewControllers([arrVC[0]], direction: .reverse, animated: true, completion: nil)
        pageController.view.semanticContentAttribute = .forceRightToLeft
        
        
        let current_index = 0
        
        for i in 0..<color_arr.count{
            if i == current_index{
                color_arr[i] = "#EB92A1"
            }else{
                color_arr[i] = "#8895A3"
            }
        }
        
        for i in 0..<color_arr_2.count{
            if i == current_index{
                arrrr[i] =  hexStringToUIColor(hex: "#EB92A1")
                //                        "#3CFDBE"
            }else{
                arrrr[i] = UIColor.clear
            }
        }
        menu_view.collView.reloadData()
        menu_view.collView.selectItem(at: IndexPath.init(item: current_index, section: 0), animated: true, scrollPosition: .centeredVertically)
        menu_view.collView.scrollToItem(at: IndexPath.init(item: current_index, section: 0), at: .centeredHorizontally, animated: true)
        


    }
    
    func presentPageVCOnView() {
        
        self.pageController = storyboard?.instantiateViewController(withIdentifier: "dcook_page_VC") as! dcook_page_VC
        self.pageController.view.frame = CGRect.init(x: 0, y: menu_view.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - menu_view.frame.maxY)
        self.addChild(self.pageController)
        self.view.addSubview(self.pageController.view)
        self.pageController.didMove(toParent: self)
        
    }
    
    func viewController(At index: Int) -> UIViewController? {
        
        if((self.menu_view.dataArray.count == 1) || (index >= self.menu_view.dataArray.count)) {
            return nil
        } else {
            let vc = self.arrVC[index]
            currentIndex = index
            return vc
        }
        
       
    }
    
    
    func nav_VC(tag: Int) {
        
        for i in 0..<arrVC.count{
            if i == tag{
                 self.pageController.setViewControllers([arrVC[i]], direction: .reverse, animated: true, completion: nil)
                 menu_view.collView.scrollToItem(at: IndexPath.init(item: tag, section: 0), at: .centeredHorizontally, animated: true)
               
                        
                        color_arr[i]  = "#EB92A1"
                         arrrr[i] =  hexStringToUIColor(hex: "#EB92A1")
//                    }else{
//                        color_arr[i] = "#8895A3"
//                         arrrr[i] = UIColor.clear
//
//                }
            } else {
                color_arr[i] = "#8895A3"
                arrrr[i] = UIColor.clear
            }
        }
         menu_view.collView.reloadData()
        
       
    }
    
    
    
    @IBAction func back_btn(_ sender: Any) {
      self.dismiss_VC()
        }
    
    
    @IBAction func menu_btn(_ sender: Any) {
         sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
  
    
 
    }


extension d_cook_container: MenuBarDelegate {
    
    func menuBarDidSelectItemAt(menu: MenuTabsView, index: Int) {
        
        // If selected Index is other than Selected one, by comparing with current index, page controller goes either forward or backward.
        
        if index != currentIndex {
            
            if index > currentIndex {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .forward, animated: true, completion: nil)
            }else {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .reverse, animated: true, completion: nil)
            }
            for i in 0..<color_arr.count{
                if i == index{
                    color_arr[i] = "#EB92A1"
                }else{
                    color_arr[i] = "#8895A3"
                }
            }
            
            for i in 0..<color_arr_2.count{
                if i == index{
                    arrrr[i] =  hexStringToUIColor(hex: "#EB92A1")
                }else{
                    arrrr[i] = UIColor.clear
                }
            }
            menu_view.collView.reloadData()
            menu_view.collView.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .centeredHorizontally, animated: true)
            
        }
        
    }
    
}


extension d_cook_container: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController)-> UIViewController? {
        curr = arrVC.firstIndex(of: viewController)!
        // if you prefer to NOT scroll circularly, simply add here:
        if curr == 0 { return nil }
        let prev = abs((curr - 1) % arrVC.count)
        
        return arrVC[prev]
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController)-> UIViewController? {
        curr = arrVC.firstIndex(of: viewController)!
        // if you prefer to NOT scroll circularly, simply add here:
        if curr == (arrVC.count - 1) { return nil }
        let nxt = abs((curr + 1) % arrVC.count)
        
        return arrVC[nxt]
    }
    
    func nextPageWithIndex(index: Int)
    {
        // let nextWalkthroughVC = newColoredViewController("setup4")
        let nextWalkthroughVC = arrVC[index]
        self.pageController.setViewControllers([nextWalkthroughVC], direction: .forward, animated: true, completion: nil)
        
    }
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if finished {
            if completed {
                
                
                let current_index = arrVC.firstIndex(of: pageViewController.viewControllers!.first!)
                
                for i in 0..<color_arr.count{
                    if i == current_index!{
                        color_arr[i] = "#EB92A1"
                    }else{
                        color_arr[i] = "#8895A3"
                    }
                }
             
                for i in 0..<color_arr_2.count{
                    if i == current_index!{
                        arrrr[i] =  hexStringToUIColor(hex: "#EB92A1")
                        
                    }else{
                        arrrr[i] = UIColor.clear
                    }
                }
                menu_view.collView.reloadData()
                menu_view.collView.selectItem(at: IndexPath.init(item: current_index!, section: 0), animated: true, scrollPosition: .centeredVertically)
                menu_view.collView.scrollToItem(at: IndexPath.init(item: current_index!, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
    }
}

