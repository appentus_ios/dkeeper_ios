//
//  tips_VC.swift
//  D.KEEPER
//
//  Created by Rajat Pathak on 12/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class tips_VC: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var catoger_one_btn: UIButton!
    @IBOutlet weak var catiger_two_btn: UIButton!
    @IBOutlet weak var catoger_three_btn: UIButton!
    @IBOutlet weak var cat_three_selected_lbl: UILabel!
    @IBOutlet weak var cat_two_selected_lbl: UILabel!
    @IBOutlet weak var cat_one_selected_lbl: UILabel!
    @IBOutlet weak var collection_view: UICollectionView!
    @IBOutlet weak var grad_view: UIView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initviews()
        // Do any additional setup after loading the view.
    }
    
    func initviews() {
        cat_three_selected_lbl.isHidden = true
        cat_two_selected_lbl.isHidden = true
        cat_one_selected_lbl.isHidden = false

        catoger_one_btn.setTitleColor(hexStringToUIColor(hex: "#EB92A1"), for: .normal)
        catiger_two_btn.setTitleColor(hexStringToUIColor(hex: "#8895A3"), for: .normal)
        catoger_three_btn.setTitleColor(hexStringToUIColor(hex: "#8895A3"), for: .normal)
        collection_view.allowsSelection = false
        grad_view.setGradientBackground(colorTop: UIColor.white.withAlphaComponent(0), colorBottom: UIColor.white)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tips_cell", for: indexPath) as! tips_cell
        cell.btn_click.addTarget(self, action: #selector(self.collection_view_tap_action(_:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collection_view.frame.width, height: 463.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 20
    }
    
    @objc func collection_view_tap_action(_ sender:UIButton){
        self.viewPresent(storyBoard: "Main", viewId: "receipes_VC")
    }

    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func side_menu_open(_ sender: UIButton) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    @IBAction func cateogeris_btn(_ sender: UIButton) {
        if sender.tag == 1{
            cat_three_selected_lbl.isHidden = true
            cat_two_selected_lbl.isHidden = true
            cat_one_selected_lbl.isHidden = false
            
            catoger_one_btn.setTitleColor(hexStringToUIColor(hex: "#EB92A1"), for: .normal)
            catiger_two_btn.setTitleColor(hexStringToUIColor(hex: "#8895A3"), for: .normal)
            catoger_three_btn.setTitleColor(hexStringToUIColor(hex: "#8895A3"), for: .normal)
        }else if sender.tag == 2{
            cat_three_selected_lbl.isHidden = true
            cat_two_selected_lbl.isHidden = false
            cat_one_selected_lbl.isHidden = true
            
            catoger_one_btn.setTitleColor(hexStringToUIColor(hex: "#8895A3"), for: .normal)
            catiger_two_btn.setTitleColor(hexStringToUIColor(hex: "#EB92A1"), for: .normal)
            catoger_three_btn.setTitleColor(hexStringToUIColor(hex: "#8895A3"), for: .normal)
        }else if sender.tag == 3{
            cat_three_selected_lbl.isHidden = false
            cat_two_selected_lbl.isHidden = true
            cat_one_selected_lbl.isHidden = true
            
            catoger_one_btn.setTitleColor(hexStringToUIColor(hex: "#8895A3"), for: .normal)
            catiger_two_btn.setTitleColor(hexStringToUIColor(hex: "#8895A3"), for: .normal)
            catoger_three_btn.setTitleColor(hexStringToUIColor(hex: "#EB92A1"), for: .normal)
        }
    }
    
    
    @IBAction func search_btn(_ sender: Any) {
        push_VC(vc_name: "search_VC", storybord_name: "DELICIOUS")
    }
    
}
