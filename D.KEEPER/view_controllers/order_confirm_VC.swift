//
//  order_confirm_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/19/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class order_confirm_VC: UIViewController {
    @IBOutlet weak var view_1: UIView!
    @IBOutlet weak var view_2: UIView!
    @IBOutlet weak var view_3: UIView!
    @IBOutlet weak var view_4: UIView!
    @IBOutlet weak var view_5: UIView!
    @IBOutlet weak var view_6: UIView!
    @IBOutlet weak var  subscription_btn: UIButton!
    @IBOutlet weak var terms_btn: UIButton!

    var delegte_call: navigate_delegate?
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        border(view: view_1, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_1, color: .clear, corner_radius: view_1.frame.height/2)
        
        border(view: view_2, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_2, color: .clear, corner_radius: view_2.frame.height/2)
        
        border(view: view_3, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_3, color: .clear, corner_radius: view_3.frame.height/2)
        
        border(view: view_4, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_4, color: .clear, corner_radius: view_4.frame.height/2)
        
        border(view: view_5, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_5, color: .clear, corner_radius: view_5.frame.height/2)
        
        border(view: view_6, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_6, color: .clear, corner_radius: view_6.frame.height/2)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func back(_ sender: Any) {
    }
    
    @IBAction func next_level(_ sender: UIButton) {
        delegte_call?.nav_VC(tag: sender.tag)
    }
    
    @IBAction func selection(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.tag == 0{
            if (sender.isSelected == true) {
                subscription_btn.setImage(UIImage(named: "Group 3360-1"), for: .normal)
                
            } else{
                subscription_btn.setImage(UIImage(named: "Ellipse 343"), for: .normal)
            }
            
        } else if sender.tag == 1 {
            if (sender.isSelected == true) {
                terms_btn.setImage(UIImage(named: "Group 3360-1"), for: .normal)
            } else{
                terms_btn.setImage(UIImage(named: "Ellipse 343"), for: .normal)
            }
        
    }
    }
    
}
