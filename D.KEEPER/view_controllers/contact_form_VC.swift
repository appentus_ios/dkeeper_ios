//
//  contact_form_VC.swift
//  D.KEEPER
//
//  Created by Rajat Pathak on 12/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class contact_form_VC: UIViewController, UITextViewDelegate {

    @IBOutlet weak var name_TF: UITextField!
    @IBOutlet weak var email_TF: UITextField!
    @IBOutlet weak var post_Text_view: UITextView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initviews()
        // Do any additional setup after loading the view.
    }
    
    func initviews() {
        name_TF.layer.masksToBounds = true
        email_TF.layer.masksToBounds = true
        post_Text_view.layer.masksToBounds = true
     
        
        name_TF.layer.cornerRadius = 22.5
        email_TF.layer.cornerRadius = 22.5
        post_Text_view.layer.cornerRadius = 22.5
      
        
        name_TF.layer.borderWidth = 1.0
        email_TF.layer.borderWidth = 1.0
        post_Text_view.layer.borderWidth = 1.0
      
        
        name_TF.layer.borderColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.54).cgColor
        email_TF.layer.borderColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.54).cgColor
        post_Text_view.layer.borderColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.54).cgColor
       
        
        name_TF.setRightPaddingPoints(20)
        email_TF.setRightPaddingPoints(20)
        post_Text_view.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10);
        post_Text_view.text = "הודעה"

    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "הודעה" {
            textView.text = nil
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "הודעה"
        }
    }
    
    @IBAction func submit_btn(_ sender: UIButton) {
//        let modalViewController = self.storyboard?.instantiateViewController(withIdentifier: "popup_submit_contact_VC") as! popup_submit_contact_VC
//        modalViewController.modalPresentationStyle = .overFullScreen
//        present(modalViewController, animated: true, completion: nil)
        
        
        let s                   = UIScreen.main.bounds.size
        let pv                  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popup_submit_contact_VC") as! popup_submit_contact_VC
        pv.view.frame           = CGRect(x:0, y:0, width: 270, height: 300)
        pv.view.backgroundColor = .clear
        popUpEffectType         = .flipUp //.zoomIn(default)/.zoomOut/.flipUp/.flipDown
        presentPopUpViewController(pv)
    }

    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func side_action(_ sender: UIButton) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)

    }
    
    
    @IBAction func search_btn(_ sender: Any) {
        push_VC(vc_name: "search_VC", storybord_name: "DELICIOUS")
    }
    
}
