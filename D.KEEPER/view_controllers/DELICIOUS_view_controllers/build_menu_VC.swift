//
//  build_menu_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/24/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class build_menu_VC: UIViewController , UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
   
    

    @IBOutlet weak var view_1: UIView!
    @IBOutlet weak var view_2: UIView!
    @IBOutlet weak var view_3: UIView!
    @IBOutlet weak var view_4: UIView!
    @IBOutlet weak var view_5: UIView!
    @IBOutlet weak var view_6: UIView!
    @IBOutlet weak var view_7: UIView!
    @IBOutlet weak var view_8: UIView!
    @IBOutlet weak var gender_TF: UITextField!
    @IBOutlet weak var veg_TF: UITextField!
    @IBOutlet weak var goal_TF: UITextField!
    @IBOutlet weak var wake_TF: UITextField!
    @IBOutlet weak var finish_TF: UITextField!
    @IBOutlet weak var don_t_TF: UITextField!
    @IBOutlet weak var training_set_TF: UITextField!
    @IBOutlet weak var training_set_up_TF: UITextField!
    
    var myPickerView : UIPickerView!
    var pickerData = ["Female" , "Male"]
    var pickerData_2 = ["Non Veg" , "Veg"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickUp(gender_TF)
        pickUp(veg_TF)
        pickUp(goal_TF)
        pickUp(wake_TF)
        pickUp(finish_TF)
        pickUp(don_t_TF)
        pickUp(training_set_TF)
        pickUp(training_set_up_TF)

        
        gender_TF.setRightPaddingPoints(10)
        veg_TF.setRightPaddingPoints(10)
        goal_TF.setRightPaddingPoints(10)
        wake_TF.setRightPaddingPoints(10)
        finish_TF.setRightPaddingPoints(10)
        don_t_TF.setRightPaddingPoints(10)
        training_set_TF.setRightPaddingPoints(10)
        training_set_up_TF.setRightPaddingPoints(10)
        
        border(view: view_1, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_1, color: .clear, corner_radius: view_1.frame.height/2)
        border(view: view_2, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_2, color: .clear, corner_radius: view_2.frame.height/2)
        border(view: view_3, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_3, color: .clear, corner_radius: view_3.frame.height/2)
        border(view: view_4, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_4, color: .clear, corner_radius: view_4.frame.height/2)
        border(view: view_5, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_5, color: .clear, corner_radius: view_5.frame.height/2)
        border(view: view_6, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_6, color: .clear, corner_radius: view_6.frame.height/2)
        border(view: view_7, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_7, color: .clear, corner_radius: view_7.frame.height/2)
        border(view: view_8, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_8, color: .clear, corner_radius: view_8.frame.height/2)
        
        

    }
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        textField.inputView = self.myPickerView
        
        // ToolBar
//        let toolBar = UIToolbar()
//        toolBar.barStyle = .default
//        toolBar.isTranslucent = true
//        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
//        toolBar.sizeToFit()
        
        // Adding Button ToolBar
//        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(build_menu_VC.doneClick))
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(build_menu_VC.cancelClick))
//        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
//        toolBar.isUserInteractionEnabled = true
//        textField.inputAccessoryView = toolBar
        
    }
    
    
    @objc func doneClick() {
        gender_TF.resignFirstResponder()
        veg_TF.resignFirstResponder()
        goal_TF.resignFirstResponder()
        wake_TF.resignFirstResponder()
        finish_TF.resignFirstResponder()
        don_t_TF.resignFirstResponder()
        training_set_TF.resignFirstResponder()
        training_set_up_TF.resignFirstResponder()

    }
//    @objc func cancelClick() {
//        gender_TF.resignFirstResponder()
//        veg_TF.resignFirstResponder()
//        goal_TF.resignFirstResponder()
//        wake_TF.resignFirstResponder()
//        finish_TF.resignFirstResponder()
//        don_t_TF.resignFirstResponder()
//        training_set_TF.resignFirstResponder()
//        training_set_up_TF.resignFirstResponder()
//    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
 
       if gender_TF.isFirstResponder == true {
        self.gender_TF.text = pickerData[row]
       
       } else if goal_TF.isFirstResponder == true{
            self.goal_TF.text = pickerData_2[row]
       
       }else if wake_TF.isFirstResponder == true{
            self.wake_TF.text = pickerData_2[row]
       
       }else if finish_TF.isFirstResponder == true{
            self.finish_TF.text = pickerData_2[row]
       
       }else if don_t_TF.isFirstResponder == true{
            self.don_t_TF.text = pickerData_2[row]
       
       }else if training_set_TF.isFirstResponder == true{
            self.training_set_TF.text = pickerData_2[row]
       
       }else{
        self.training_set_up_TF.text = pickerData_2[row]
        
        }

    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if gender_TF.isFirstResponder == true {
        return pickerData[row]
        } else {
            return pickerData_2[row]
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if gender_TF.isFirstResponder == true {
             self.pickUp(gender_TF)
            
        } else if goal_TF.isFirstResponder == true{
            self.pickUp(goal_TF)
            
        }else if wake_TF.isFirstResponder == true{
            self.pickUp(wake_TF)
            
        }else if finish_TF.isFirstResponder == true{
            self.pickUp(finish_TF)
        
        }else if don_t_TF.isFirstResponder == true{
             self.pickUp(don_t_TF)
            
        }else if training_set_TF.isFirstResponder == true{
             self.pickUp(training_set_TF)
            
        }else{
             self.pickUp(training_set_up_TF)
            
        }
        
    }
    
    @IBAction func add_btn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "personal"), object:nil)

        
    }
    
    @IBAction func history_btn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "week_menu"), object:nil)

        
    }
    
    @IBAction func ask_btn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "chat"), object:nil)

    }
    
    
}
