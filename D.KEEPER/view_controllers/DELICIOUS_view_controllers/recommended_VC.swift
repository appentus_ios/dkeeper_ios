//
//  recommended_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/30/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class recommended_VC: UIViewController {

    @IBOutlet weak var pro_img: UIImageView!
    @IBOutlet weak var notify_view: UIView!


    
    let pulsator = Pulsator()
    let kMaxRadius: CGFloat = 200
    let kMaxDuration: TimeInterval = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pro_img.layer.superlayer?.insertSublayer(pulsator, below: pro_img.layer)
        pulsator.position = pro_img.layer.position
        
        pulsator.numPulse = 8
        pulsator.radius = 0.6 * kMaxRadius
        pulsator.animationDuration = 0.8 * kMaxDuration
        pulsator.backgroundColor = UIColor.init(red: 200.0/255.0, green: 212.0/255.0, blue: 230.0/255.0, alpha: 1.0).cgColor
        pulsator.start()
        
        
        sideMenuController?.rightViewPresentationStyle = .scaleFromBig
        sideMenuController?.rightViewStatusBarStyle = .lightContent
    }
    

    @IBAction func hide_btn(_ sender: Any) {
        
        notify_view.isHidden = true
    }
    
    @IBAction func back_btn(_ sender: UIButton){
        self.dismiss_VC()
    }
    
    @IBAction func menu_btn(_ sender: UIButton){
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
}
    
}

extension recommended_VC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recommended_Table_Cell", for: indexPath) as! recommended_Table_Cell
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
}
