//
//  weight_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/24/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class weight_VC: UIViewController {
    
    @IBOutlet weak var view_1:UIView!
    @IBOutlet weak var view_2:UIView!
    @IBOutlet weak var view_3:UIView!
    @IBOutlet weak var view_4:UIView!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        border(view: view_3, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_3, color: .clear, corner_radius: view_3.frame.height/2)
        
        border(view: view_4, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_4, color: .clear, corner_radius: view_4.frame.height/2)
        view_1.layer.cornerRadius = 4
        view_1.clipsToBounds = true
        
        view_2.layer.cornerRadius = 4
        view_2.clipsToBounds = true
    }
    
    
    @IBAction func addimg_1_btn(_ sender: Any) {
        //        UserDefaults.standard.set(true, forKey: "once_from_dfit")
        
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "weight2"), object:nil)
    }
    
    @IBAction func plus_btn(_ sender: Any) {
       
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "tracking"), object:nil)
    }
    
    @IBAction func history_btn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "week"), object:nil)
    }
    
}
