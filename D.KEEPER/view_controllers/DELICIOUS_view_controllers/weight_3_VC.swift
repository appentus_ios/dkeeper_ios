//
//  weight_3_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/25/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class weight_3_VC: UIViewController {

    
    @IBOutlet weak var view_1:UIView!
    @IBOutlet weak var view_2:UIView!
   
    @IBOutlet weak var current_weight_TF: UITextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        current_weight_TF.setRightPaddingPoints(10)
        
        
        view_1.layer.cornerRadius = 4
        view_1.clipsToBounds = true
        border(view: view_2, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_2, color: .clear, corner_radius: view_2.frame.height/2)
        
    }
    
    

}
