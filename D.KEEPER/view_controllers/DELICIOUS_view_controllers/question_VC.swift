//
//  question_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/26/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit
import Koloda

class question_VC: UIViewController {

    @IBOutlet weak var view_1: KolodaView!
    @IBOutlet weak var view_2: UIView!
    @IBOutlet weak var view_3: UIView!
    @IBOutlet weak var page_control: UIPageControl!
    @IBOutlet weak var title_Lbl: UILabel!
    @IBOutlet weak var vegan_lbl: UILabel!
    
    
    var title_arr = ["נבהיר כמה פרטים קטנים  לפני שנתחיל"
    , "נבהיר כמה פרטים קטנים  לפני שנתחיל"
    , "כל אחד והעדפות שלו   נדאג להמליץ לכם רק לפי העדפות שלכם "
    , "בחרתם בהעדפה שלא הוספנו ברשימה "
    , "טיפוס של בית חובב מסעדות "
    , "גיל הוא חלק מחישוב המטבוליזם "
    , "משקל "
    , "גובה הוא חלק מחישוב המטבוליזם "]
    
    var cards_total = 8
//   var current_index = 0
    override func viewDidLoad() {
        super.viewDidLoad()
     
        view_2.layer.cornerRadius = 4
        view_2.clipsToBounds = true
        
        view_3.layer.cornerRadius = 4
        view_3.clipsToBounds = true
     
        view_1.dataSource = self
        view_1.delegate = self
        self.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
        page_control.currentPage = 0
        view_1.visibleCardsDirection = .bottom

    }
    
    override func viewWillAppear(_ animated: Bool){
                page_control.numberOfPages = 8
                view_1.resetCurrentCardIndex()
    }

    @IBAction func back(_ sender: Any){
        dismiss_VC()
        
    }
    
    @IBAction func menu_btn(_ sender: Any) {
         sideMenuController?.showRightView(animated: true, completionHandler: nil)
        
    }
    
    @IBAction func leftButtonTapped(_ sender: UIButton) {
        view_1?.swipe(.left)
    }

    @IBAction func rightButtonTapped(_ sender: UIButton) {
        view_1?.swipe(.right)
    }
}

extension question_VC: KolodaViewDelegate {
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        self.viewPresent(storyBoard: "Main", viewId: "LGSideMenuController")
        
    }

    
}

// MARK: KolodaViewDataSource

extension question_VC: KolodaViewDataSource {
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return cards_total
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .default
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        
        if index == 0{
        let first_swipe_view: first_swipe_view = UIView.fromNib()
           shadow_on_view(view: first_swipe_view, color: .black, corner_radius: 4)
            first_swipe_view.layer.masksToBounds = true
            first_swipe_view.left_Btn.addTarget(self, action: #selector(leftButtonTapped(_:)), for: .touchUpInside)
            first_swipe_view.right_Btn.addTarget(self, action: #selector(rightButtonTapped(_:)), for: .touchUpInside)
            
            return first_swipe_view
        } else if index == 1{
            let second_question_view: second_question_view = UIView.fromNib()
            shadow_on_view(view: second_question_view, color: .black, corner_radius: 4)
            second_question_view.layer.masksToBounds = true
            second_question_view.left_Btn.addTarget(self, action: #selector(leftButtonTapped(_:)), for: .touchUpInside)
            second_question_view.right_btn.addTarget(self, action: #selector(rightButtonTapped(_:)), for: .touchUpInside)
            
            return second_question_view
        
        } else if index == 2 {
         
            let third_question_view: third_question_view = UIView.fromNib()
            shadow_on_view(view: third_question_view, color: .black, corner_radius: 4)
            third_question_view.layer.masksToBounds = true
            third_question_view.right_Btn.addTarget(self, action: #selector(rightButtonTapped(_:)), for: .touchUpInside)
        
            return third_question_view
        
        } else if index == 3{
            
            let fourth_question_view: fourth_question_view = UIView.fromNib()
            shadow_on_view(view: fourth_question_view, color: .black, corner_radius: 4)
            fourth_question_view.layer.masksToBounds = true
            fourth_question_view.right_Btn.addTarget(self, action: #selector(rightButtonTapped(_:)), for: .touchUpInside)
            return fourth_question_view
        } else if index == 4 {
            let fifth_question_view: fifth_Question_View = UIView.fromNib()
            shadow_on_view(view: fifth_question_view, color: .black, corner_radius: 4)
            fifth_question_view.layer.masksToBounds = true
            fifth_question_view.left_Btn.addTarget(self, action: #selector(leftButtonTapped(_:)), for: .touchUpInside)
            fifth_question_view.right_btn.addTarget(self, action: #selector(rightButtonTapped(_:)), for: .touchUpInside)
            return fifth_question_view
            
        } else if index == 5{
            let sixth_question_view: sixth_Question_View = UIView.fromNib()
            shadow_on_view(view: sixth_question_view, color: .black, corner_radius: 4)
            sixth_question_view.layer.masksToBounds = true
            sixth_question_view.right_Btn.addTarget(self, action: #selector(rightButtonTapped(_:)), for: .touchUpInside)
            return sixth_question_view
            
        } else if index == 6
            {
            let seventh_question_view: seventh_Question_View = UIView.fromNib()
            shadow_on_view(view: seventh_question_view, color: .black, corner_radius: 4)
            seventh_question_view.layer.masksToBounds = true
            seventh_question_view.right_Btn.addTarget(self, action: #selector(rightButtonTapped(_:)), for: .touchUpInside)
            return seventh_question_view
            
        } else {
            let eighth_question_view: eighth_Question_View = UIView.fromNib()
            shadow_on_view(view: eighth_question_view, color: .black, corner_radius: 4)
            eighth_question_view.layer.masksToBounds = true
            eighth_question_view.right_Btn.addTarget(self, action: #selector(rightButtonTapped(_:)), for: .touchUpInside)
            return eighth_question_view
       
        }
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        
        if index == 0 {
            return Bundle.main.loadNibNamed("OverlayView", owner: self, options: nil)?[0] as? OverlayView
        } else if index == 1 {
            return Bundle.main.loadNibNamed("overlay_view_2", owner: self, options: nil)?[0] as? OverlayView
        } else if index == 4 {
            return Bundle.main.loadNibNamed("overlay_View_3", owner: self, options: nil)?[0] as? OverlayView
        } else {
            return nil
        }
    }
    
    func koloda(_ koloda: KolodaView, didShowCardAt index: Int) {
        page_control.currentPage = index
        title_Lbl.text = title_arr[index]
        
        for i in 0..<index{
            if i == 1{
                vegan_lbl.isHidden = false
            } else {
                vegan_lbl.isHidden = true
            }
        }
        
}
    
}
