//
//  personal_menu_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/24/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class personal_menu_VC: UIViewController {

    @IBOutlet weak var table_1: UITableView!
    @IBOutlet weak var table_2: UITableView!


    
    override func viewDidLoad() {
        super.viewDidLoad()
   

    }
    
    @IBAction func ask_btn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "chat"), object:nil)

    }
    
    
    @IBAction func history_btn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "week_menu"), object:nil)

    }
    
    
    
    @IBAction func add_btn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "build_menu"), object:nil)

    }
    
   
}


extension personal_menu_VC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == table_1{
           
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "personal_menu_1_TableCell", for: indexPath) as! personal_menu_1_TableCell
            
            return cell
            
        } else {
            let cell_2 = tableView.dequeueReusableCell(withIdentifier: "personal_menu_2_TableCell", for: indexPath) as! personal_menu_2_TableCell
           
            if indexPath.row % 2 == 0 {
                cell_2.view_1.backgroundColor = UIColor.init(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1)
                cell_2.time_Lbl.textColor = UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1)
                cell_2.wake_lbl.textColor = UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1)
                cell_2.detail_lbl.textColor = UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1)
                cell_2.date_imgView.setBackgroundImage(UIImage(named: "clock_button"), for: .normal)
                cell_2.detail_Img.setBackgroundImage(UIImage(named: "info_button-3"), for: .normal)
                cell_2.bar_lbl.backgroundColor = UIColor.white
                
               

            }else{
                cell_2.view_1.backgroundColor = UIColor.init(red: 235.0/255.0, green: 146.0/255.0, blue: 161.0/255.0, alpha: 1)
                cell_2.time_Lbl.textColor = UIColor.white
                cell_2.wake_lbl.textColor = UIColor.white
                cell_2.detail_lbl.textColor = UIColor.white
                cell_2.date_imgView.setBackgroundImage(UIImage(named: "clock_pink_button"), for: .normal)
                cell_2.detail_Img.setBackgroundImage(UIImage(named: "info_pink_button"), for: .normal)
                cell_2.bar_lbl.backgroundColor = UIColor.white
                
            }
            
            
            return cell_2
            
            
        }
}

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == table_1{
            return 90
        } else {
            return 120        }
}
}
