//
//  Weeks_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/24/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class Weeks_VC: UIViewController {

    @IBOutlet weak var tblview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func selection(_ sender: UIButton){
        
        sender.isSelected = true
        self.tblview.reloadData()
        
        
    }

    @IBAction func add_btn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "weight"), object:nil)

    }
    
    @IBAction func history_btn(_ sender: Any) {
        
    }
    @IBAction func ask_btn(_ sender: Any) {
        
       
    }
}


extension Weeks_VC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "weeks_TableCell", for: indexPath) as! weeks_TableCell
        cell.button.addTarget(self, action: #selector(selection(_:)), for: .touchUpInside)
        cell.button.tag = indexPath.row
        if cell.button.isSelected == true {
            cell.view_1.backgroundColor = hexStringToUIColor(hex: "#8895A3")
            cell.button.isSelected = false
        } else {
            cell.view_1.backgroundColor = hexStringToUIColor(hex: "#EB92A1")
            cell.button.isSelected = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78
    }
    
    
    
    
}
