//
//  d_fit_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/25/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit
    var color_arr = ["#EB92A1","#8895A3","#8895A3","#8895A3","#8895A3","#8895A3","#8895A3","#8895A3","#8895A3"]
    var color_arr_2 = ["","","","","","","","",""]
    var back_arr = ["ADI", "ADI", "WEEK4"]
    var arrrr : [UIColor] = [hexStringToUIColor(hex: "#3CFDBE"),UIColor.clear,UIColor.clear,UIColor.clear,UIColor.clear,UIColor.clear,UIColor.clear,UIColor.clear,UIColor.clear]


    class d_fit_VC: UIViewController{

        
        
    @IBOutlet weak var back_lbl: UILabel!
    @IBOutlet weak var menu_bar: MenuTabsView!
    var once = false
    var curr = Int()
    var currentIndex: Int = 0
    var tabs = ["הסבר כללי","מעקב","סרטונים"]
    var pageController: UIPageViewController!
    
    var arrVC = [UIViewController]()
    
    override func viewDidLoad() {
     
        super.viewDidLoad()
        curr = 0
        let d_fit_genExp = storyboard?.instantiateViewController(withIdentifier: "d_fit_generalexplation_VC") as! d_fit_generalexplation_VC
        let tracking_vc = storyboard?.instantiateViewController(withIdentifier: "d_fit_tracking_VC") as! d_fit_tracking_VC
        let tracking_vc_ = storyboard?.instantiateViewController(withIdentifier: "video_track_2_VC") as! video_track_2_VC

        arrVC = [d_fit_genExp,tracking_vc,tracking_vc_]
        self.menu_bar.semanticContentAttribute = .forceRightToLeft
        menu_bar.dataArray = tabs
        menu_bar.isSizeToFitCellsNeeded = true
        menu_bar.collView.backgroundColor = .clear
        presentPageVCOnView()
        
        menu_bar.menuDelegate = self
        pageController.delegate = self
        pageController.dataSource = self
        menu_bar.collView.semanticContentAttribute = .forceRightToLeft
        //For Intial Display
        
//        menu_bar.collView.scrollToItem(at: IndexPath.init(item: 0, section: 0), at: .right, animated: true)

//        menu_bar.collView.selectItem(at: IndexPath.init(item: 0, section: 0), animated: true, scrollPosition: .right)
        pageController.setViewControllers([arrVC[0]], direction: .reverse, animated: true, completion: nil)
        
        sideMenuController?.rightViewPresentationStyle = .scaleFromBig
        sideMenuController?.rightViewStatusBarStyle = .lightContent
        pageController.view.semanticContentAttribute = .forceRightToLeft

         NotificationCenter.default.addObserver(self, selector: #selector(btn_history), name: NSNotification.Name (rawValue: "tracking"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(add_img), name: NSNotification.Name (rawValue: "weight2"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(video), name: NSNotification.Name (rawValue: "video"), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(video_play), name: NSNotification.Name (rawValue: "video_play"), object: nil)
        
        
        let current_index = 0
        
        for i in 0..<color_arr.count{
            if i == current_index{
                color_arr[i] = "#EB92A1"
            }else{
                color_arr[i] = "#8895A3"
            }
        }
        for i in 0..<color_arr_2.count{
            if i == current_index{
                arrrr[i] =  hexStringToUIColor(hex: "#EB92A1")
                
            }else{
                arrrr[i] = UIColor.clear
            }
        }
        menu_bar.collView.reloadData()
        menu_bar.collView.selectItem(at: IndexPath.init(item: current_index, section: 0), animated: true, scrollPosition: .centeredVertically)
        menu_bar.collView.scrollToItem(at: IndexPath.init(item: current_index, section: 0), at: .centeredHorizontally, animated: true)
        
        
}
    override func viewWillAppear(_ animated: Bool) {
        pageController.dataSource = self
    }
    
    func presentPageVCOnView() {
        
        self.pageController = storyboard?.instantiateViewController(withIdentifier: "page_VC") as! page_VC
        self.pageController.view.frame = CGRect.init(x: 0, y: menu_bar.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - menu_bar.frame.maxY)
        self.addChild(self.pageController)
        self.view.addSubview(self.pageController.view)
        self.pageController.didMove(toParent: self)
        
    }
    func viewController(At index: Int) -> UIViewController? {
        
        if((self.menu_bar.dataArray.count == 1) || (index >= self.menu_bar.dataArray.count)) {
            return nil
        } else {
                let vc = self.arrVC[index]
                currentIndex = index
                return vc
        }
    }
    
    
    @IBAction func menu_btn(_ sender: UIButton){
         sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
    @IBAction func back(_ sender: Any){
        dismiss_VC()
    }
}
    extension d_fit_VC: MenuBarDelegate {
    
    func menuBarDidSelectItemAt(menu: MenuTabsView, index: Int) {
        
        // If selected Index is other than Selected one, by comparing with current index, page controller goes either forward or backward.
        
        if index != currentIndex {
            
            if index > currentIndex {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .forward, animated: true, completion: nil)
            }else {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .reverse, animated: true, completion: nil)
            }
            for i in 0..<color_arr.count{
                if i == index{
                    color_arr[i] = "#EB92A1"
                }else{
                    color_arr[i] = "#8895A3"
                }
            }
            
            for i in 0..<color_arr_2.count{
                if i == index{
                    arrrr[i] =  hexStringToUIColor(hex: "#EB92A1")
                }else{
                    arrrr[i] = UIColor.clear
                }
            }
            menu_bar.collView.reloadData()
            menu_bar.collView.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .centeredHorizontally, animated: true)
            
        }
        
    }
    
}


extension d_fit_VC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController)-> UIViewController? {
        curr = arrVC.firstIndex(of: viewController) ?? 0
        // if you prefer to NOT scroll circularly, simply add here:
        if curr == 0 { return nil }
        let prev = abs((curr - 1) % arrVC.count)

        return arrVC[prev]
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController)-> UIViewController? {
        curr = arrVC.firstIndex(of: viewController) ?? 0
        // if you prefer to NOT scroll circularly, simply add here:
        if curr == (arrVC.count - 1) { return nil }
        let nxt = abs((curr + 1) % arrVC.count)

        return arrVC[nxt]
    }
    
    func nextPageWithIndex(index: Int)
    {
        // let nextWalkthroughVC = newColoredViewController("setup4")
        let nextWalkthroughVC = arrVC[index]
        self.pageController.setViewControllers([nextWalkthroughVC], direction: .forward, animated: true, completion: nil)
        
    }
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {

        if finished {
            if completed {
                
              
                let current_index = arrVC.firstIndex(of: pageViewController.viewControllers!.first!)
                
                for i in 0..<color_arr.count{
                    if i == current_index{
                         back_lbl.text = back_arr[current_index!]
                        color_arr[i] = "#EB92A1"
                    }else{
                        color_arr[i] = "#8895A3"
                    }
                }
                for i in 0..<color_arr_2.count{
                    if i == current_index{
                        arrrr[i] =  hexStringToUIColor(hex: "#EB92A1")
                       
                    }else{
                        arrrr[i] = UIColor.clear
                    }
                }
                menu_bar.collView.reloadData()
                menu_bar.collView.selectItem(at: IndexPath.init(item: current_index ?? 0, section: 0), animated: true, scrollPosition: .centeredVertically)
                menu_bar.collView.scrollToItem(at: IndexPath.init(item: current_index ?? 0, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
    }
}

extension d_fit_VC {
    @objc func btn_history() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "weight_VC") as! weight_VC
            self.arrVC[1] = vc
            self.nextPageWithIndex(index: 1)
        }
    }
    
    @objc func add_img() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "weight_2_VC") as! weight_2_VC
//            vc.img_view.image = UIImage(named: "graphics_11")
            self.arrVC[1] = vc
            self.nextPageWithIndex(index: 1)
        }
        
    }
  
    @objc func video() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "video_track_3_VC") as! video_track_3_VC
            //            vc.img_view.image = UIImage(named: "graphics_11")
            self.arrVC[2] = vc
            self.nextPageWithIndex(index: 2)
        }
        
    }
    
    @objc func video_play() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "video_track_4_VC") as! video_track_4_VC
            //            vc.img_view.image = UIImage(named: "graphics_11")
            self.arrVC[2] = vc
            self.nextPageWithIndex(index: 2)
        }
        
    }
}


