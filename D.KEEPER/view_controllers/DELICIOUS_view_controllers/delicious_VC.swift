//
//  delicious_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/23/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class delicious_VC: UIViewController {
    
   
    @IBOutlet weak var menu_view: MenuTabsView!
    @IBOutlet weak var back_lbl: UILabel!
   
    var back_arr = ["ADI", "ADI", "WEEK4", "SEP", "SHOP"]
    var once = false
    var curr = Int()
    var currentIndex: Int = 0
    var tabs = [
            "הסבר כללי"
  ,
                "מעקב"
 ,
                "תפריט שבועי"
,
                "יומן אכילה"
,
                "רשימת קניות"
    ]
    var pageController: UIPageViewController!
    
    var arrVC = [UIViewController]()

    override func viewDidLoad() {
        super.viewDidLoad()

        curr = 0
        let controller_1  = storyboard?.instantiateViewController(withIdentifier: "general_explanation_VC") as! general_explanation_VC
        let controller_2  = storyboard?.instantiateViewController(withIdentifier: "tracking_VC") as! tracking_VC
        let controller_3 =  storyboard?.instantiateViewController(withIdentifier: "personal_menu_VC") as! personal_menu_VC
        let controller_4  = storyboard?.instantiateViewController(withIdentifier: "menu_day_book_VC") as! menu_day_book_VC
        let controller_5 = storyboard?.instantiateViewController(withIdentifier: "shooping_list_VC") as! shooping_list_VC
        
        arrVC = [controller_1,controller_2,controller_3,controller_4,controller_5]
        
        menu_view.dataArray = tabs
        menu_view.isSizeToFitCellsNeeded = true
        menu_view.collView.backgroundColor = .clear
        presentPageVCOnView()
        
        menu_view.menuDelegate = self
        pageController.delegate = self
        pageController.dataSource = self

        //For Intial Display
//        menu_view.collView.selectItem(at: IndexPath.init(item: 0, section: 0), animated: true, scrollPosition: .right)
//        menu_view.collView.scrollToItem(at: IndexPath.init(item: 0, section: 0), at: .right, animated: true)
        pageController.setViewControllers([arrVC[0]], direction: .forward, animated: true, completion: nil)
        pageController.view.semanticContentAttribute = .forceRightToLeft
//        sideMenuController?.rightViewPresentationStyle = .scaleFromBig
//        sideMenuController?.rightViewStatusBarStyle = .lightContent
       
        NotificationCenter.default.addObserver(self, selector: #selector(btn_history), name: NSNotification.Name (rawValue: "weight"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(add_img), name: NSNotification.Name (rawValue: "weight2"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(week_btn), name: NSNotification.Name (rawValue: "week"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(week_menu), name: NSNotification.Name (rawValue: "week_menu"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(build_menu), name: NSNotification.Name (rawValue: "build_menu"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(menu_eating), name: NSNotification.Name (rawValue: "menu_eating"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(tracking), name: NSNotification.Name (rawValue: "tracking"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(personal), name: NSNotification.Name (rawValue: "personal"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(edit_shopping), name: NSNotification.Name (rawValue: "edit_shopping"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(shopping), name: NSNotification.Name (rawValue: "shopping"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(menu_day), name: NSNotification.Name (rawValue: "menu_day"), object: nil)
        
        
        
        
        
        
        let current_index = 0
        
        for i in 0..<color_arr.count{
            if i == current_index{
                color_arr[i] = "#EB92A1"
            }else{
                color_arr[i] = "#8895A3"
            }
        }
        
        for i in 0..<color_arr_2.count{
            if i == current_index{
                arrrr[i] =  hexStringToUIColor(hex: "#EB92A1")
            }else{
                arrrr[i] = UIColor.clear
            }
        }
        menu_view.collView.reloadData()
        menu_view.collView.selectItem(at: IndexPath.init(item: current_index, section: 0), animated: true, scrollPosition: .centeredVertically)
        menu_view.collView.scrollToItem(at: IndexPath.init(item: current_index, section: 0), at: .centeredHorizontally, animated: true)

        
    }
    
    func presentPageVCOnView() {
        
        self.pageController = storyboard?.instantiateViewController(withIdentifier: "delicious_page_VC") as! delicious_page_VC
        self.pageController.view.frame = CGRect.init(x: 0, y: menu_view.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - menu_view.frame.maxY)
        self.addChild(self.pageController)
        self.view.addSubview(self.pageController.view)
        self.pageController.didMove(toParent: self)
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super .viewWillAppear(animated)
//        
//        menu_view.collView.selectItem(at: IndexPath.init(item: 0, section: 0), animated: true, scrollPosition: .right)
//        menu_view.collView.scrollToItem(at: IndexPath.init(item: 0, section: 0), at: .right, animated: true)
//        
//    }
    
    func viewController(At index: Int) -> UIViewController? {
        
        if((self.menu_view.dataArray.count == 1) || (index >= self.menu_view.dataArray.count)) {
            return nil
        } else {
            let vc = self.arrVC[index]
            
            currentIndex = index
            return vc
        }
    }
    
    @IBAction func menu_btn(_ sender: Any) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }

    func didMoveToPage(_ controller: UIViewController, index: Int) {
    
    }
    
    @IBAction func back(_ sender: UIButton){
        self.dismiss_VC()
    }
    
}

extension delicious_VC: MenuBarDelegate {
    
    func menuBarDidSelectItemAt(menu: MenuTabsView, index: Int) {
        
        // If selected Index is other than Selected one, by comparing with current index, page controller goes either forward or backward.
        
        if index != currentIndex {
            
            if index > currentIndex {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .forward, animated: true, completion: nil)
            }else {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .reverse, animated: true, completion: nil)
            }
            for i in 0..<color_arr.count{
                if i == index{
                    color_arr[i] = "#EB92A1"
                }else{
                    color_arr[i] = "#8895A3"
                }
            }
            
            for i in 0..<color_arr_2.count{
                if i == index{
                    arrrr[i] =  hexStringToUIColor(hex: "#EB92A1")
                }else{
                    arrrr[i] = UIColor.clear
                }
            }
            
//            menu_view.collView.reloadData()
            menu_view.collView.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .centeredHorizontally, animated: true)
           
            menu_view.collView.reloadData()
        }
        
    }
    
}


extension delicious_VC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController)-> UIViewController? {
        curr = arrVC.firstIndex(of: viewController) ?? 0
        // if you prefer to NOT scroll circularly, simply add here:
        if curr == 0 { return nil }
        let prev = abs((curr - 1) % arrVC.count)
        
        return arrVC[prev]
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController)-> UIViewController? {
        curr = arrVC.firstIndex(of: viewController) ?? 0
        // if you prefer to NOT scroll circularly, simply add here:
        if curr == (arrVC.count - 1) { return nil }
        let nxt = abs((curr + 1) % arrVC.count)
        return arrVC[nxt]
    
    }
    
    func nextPageWithIndex(index: Int)
    {
        // let nextWalkthroughVC = newColoredViewController("setup4")
        let nextWalkthroughVC = arrVC[index]
       
        self.pageController.setViewControllers([nextWalkthroughVC], direction: .forward, animated: true, completion: nil)
        
    }
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if finished {
            if completed {
                
                
                let current_index = arrVC.firstIndex(of: pageViewController.viewControllers!.first!)
//
                for i in 0..<color_arr.count{
                    if i == current_index{
                        back_lbl.text = back_arr[current_index!]
                        color_arr[i] = "#EB92A1"
                    }else{
                        color_arr[i] = "#8895A3"
                    }
                }

                for i in 0..<color_arr_2.count{
                    if i == current_index{
                        arrrr[i] =  hexStringToUIColor(hex: "#EB92A1")
                    }else{
                        arrrr[i] = UIColor.clear
                    }
                }
                menu_view.collView.reloadData()
                menu_view.collView.selectItem(at: IndexPath.init(item: current_index ?? 0, section: 0), animated: true, scrollPosition: .centeredVertically)
                menu_view.collView.scrollToItem(at: IndexPath.init(item: current_index ?? 0, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
    }
}



extension delicious_VC {
    @objc func btn_history() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "weight_VC") as! weight_VC
            self.arrVC[1] = vc
            self.nextPageWithIndex(index: 1)
        }
    }
    
    @objc func add_img() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "weight_2_VC") as! weight_2_VC
            
            self.arrVC[1] = vc
            self.nextPageWithIndex(index: 1)
        }
    }
    @objc func week_btn() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Weeks_VC") as! Weeks_VC
            self.arrVC[1] = vc
            self.nextPageWithIndex(index: 1)
        }
        
    }
    @objc func tracking() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "tracking_VC") as! tracking_VC
            self.arrVC[1] = vc
            self.nextPageWithIndex(index: 1)
            
        }
    }
    @objc func personal() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "personal_menu_VC") as! personal_menu_VC
            self.arrVC[2] = vc
            self.nextPageWithIndex(index: 2)
            
        }
    }
    
    
    @objc func week_menu() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "week_menu_VC") as! week_menu_VC
            self.arrVC[2] = vc
            self.nextPageWithIndex(index: 2)
        }
        
    }
    
    @objc func build_menu() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "build_menu_VC") as! build_menu_VC
            self.arrVC[2] = vc
            self.nextPageWithIndex(index: 2)
        }
        
    }
    
    @objc func menu_eating() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "menu_eating_VC") as! menu_eating_VC
            self.arrVC[3] = vc
            self.nextPageWithIndex(index: 3)
            
        }
    }
    @objc func menu_day() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "menu_day_book_VC") as! menu_day_book_VC
            self.arrVC[3] = vc
            self.nextPageWithIndex(index: 3)
            
        }
    }
    
   @objc func edit_shopping() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "edit_shopping_VC") as! edit_shopping_VC
            self.arrVC[4] = vc
            self.nextPageWithIndex(index: 4)
            
        }
    }
    
    @objc func shopping() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "shooping_list_VC") as! shooping_list_VC
            self.arrVC[4] = vc
            self.nextPageWithIndex(index: 4)
            
        }
    }
  
    
    
    
    
}
