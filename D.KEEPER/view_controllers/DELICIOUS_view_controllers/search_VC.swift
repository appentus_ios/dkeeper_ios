//
//  search_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/26/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class search_VC: UIViewController {
    
    @IBOutlet weak var search_TF: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        border(view: search_TF, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: search_TF, color: .clear, corner_radius: search_TF.frame.height/2)
        
        
        search_TF.setRightPaddingPoints(15)
        search_TF.setLeftPaddingPoints(40)

    }
    
    @IBAction func back(_ sender: Any){
        self.dismiss_VC()
    }
    
    @IBAction func menu_btn(_ sender: Any){
       sideMenuController?.showRightView(animated: true, completionHandler: nil) 
    }
    
    
    @IBAction func search(_ sender: Any) {
        
        
        
        let s                   = UIScreen.main.bounds.size
        let pv                  = UIStoryboard(name: "DELICIOUS", bundle: nil).instantiateViewController(withIdentifier: "no_result_VC") as! no_result_VC
        pv.view.frame           = CGRect(x:0, y:0, width: 270, height: 300)
        pv.view.backgroundColor = .clear
        popUpEffectType         = .zoomIn //.zoomIn(default)/.zoomOut/.flipUp/.flipDown
        presentPopUpViewController(pv)
        
//        let modalViewController = self.storyboard?.instantiateViewController(withIdentifier: "") as! no_result_VC
//        modalViewController.modalPresentationStyle = .overFullScreen
//        present(modalViewController, animated: true, completion: nil)

        
    }
    
    @IBAction func filter_btn(_ sender: Any){
        push_VC(vc_name: "search_restaurant_VC", storybord_name: "DELICIOUS")
    }
    
    @IBAction func dish_found_btn(_ sender: Any){
        push_VC(vc_name: "request_restaurant_VC", storybord_name: "DELICIOUS")

    }
    
}
