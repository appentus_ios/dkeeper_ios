//
//  health_statement_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/25/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class health_statement_VC: UIViewController {
    
    @IBOutlet weak var  health_issue: UIButton!
    @IBOutlet weak var medication: UIButton!
    @IBOutlet weak var pergent: UIButton!
    @IBOutlet weak var age: UIButton!
    @IBOutlet weak var birth: UIButton!


    var delegate: navigate_delegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func selection(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.tag == 0{
            if (sender.isSelected == true) {
                health_issue.setImage(UIImage(named: "Group 3360-1"), for: .normal)
                
            } else{
                health_issue.setImage(UIImage(named: "Ellipse 343"), for: .normal)
            }
            
        } else if sender.tag == 1 {
            if (sender.isSelected == true) {
                medication.setImage(UIImage(named: "Group 3360-1"), for: .normal)
            } else{
                medication.setImage(UIImage(named: "Ellipse 343"), for: .normal)
            }
            
        } else if sender.tag == 2 {
            
            if (sender.isSelected == true) {
                pergent.setImage(UIImage(named: "Group 3360-1"), for: .normal)
            } else{
                pergent.setImage(UIImage(named: "Ellipse 343"), for: .normal)
            }
            
        }else if sender.tag == 3 {
            
            if (sender.isSelected == true) {
                age.setImage(UIImage(named: "Group 3360-1"), for: .normal)
            } else{
                age.setImage(UIImage(named: "Ellipse 343"), for: .normal)
            }
            
        }else{
            if (sender.isSelected == true) {
                birth.setImage(UIImage(named: "Group 3360-1"), for: .normal)
            } else{
                birth.setImage(UIImage(named: "Ellipse 343"), for: .normal)
            }
        }
    }
    
    
    @IBAction func next_step_Btn(_ sender: UIButton) {
       
        delegate?.nav_VC(tag: sender.tag)
   
    }
    
   

}
