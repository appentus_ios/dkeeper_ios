//
//  confirm_program_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/25/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit
import BubbleTransition

class confirm_program_VC: UIViewController {
   
    @IBOutlet weak var round_title_view:UIView!
    @IBOutlet weak var  facebook_Btn: UIButton!
    @IBOutlet weak var kit_Btn: UIButton!
    @IBOutlet weak var start_Btn: UIButton!


    
   
    
    let transition = BubbleTransition()
    let interactiveTransition = BubbleInteractiveTransition()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        round_title_view.layer.cornerRadius = round_title_view.bounds.height/2
        round_title_view.clipsToBounds = true
    }
    
   
    
    @IBAction func selection(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            sender.isSelected = true
        } else {
             sender.isSelected = false
        }
        
//        if sender.tag == 0{
//            if (sender.isSelected == true) {
//                facebook_Btn.setImage(UIImage(named: "checkmark_green"), for: .normal)
//
//            } else{
//                facebook_Btn.setImage(UIImage(named: "Ellipse 343"), for: .normal)
//            }
//
//        } else if sender.tag == 1 {
//            if (sender.isSelected == true) {
//                kit_Btn.setImage(UIImage(named: "checkmark_green"), for: .normal)
//            } else{
//                kit_Btn.setImage(UIImage(named: "Ellipse 343"), for: .normal)
//            }
//
//        } else if sender.tag == 2 {
//
//            if (sender.isSelected == true) {
//                start_Btn.setImage(UIImage(named: "checkmark_green"), for: .normal)
//            } else{
//                start_Btn.setImage(UIImage(named: "Ellipse 343"), for: .normal)
//            }
//
//        }
    }
    
    @IBAction func date(_ sender: Any){
        let s                   = UIScreen.main.bounds.size
        let pv                  = UIStoryboard(name: "DELICIOUS", bundle: nil).instantiateViewController(withIdentifier: "calendar_popup_VC") as! calendar_popup_VC
        pv.view.frame           = CGRect(x:0, y:0, width: 300 , height: 270)
        pv.view.backgroundColor = .clear
        popUpEffectType         = .zoomOut //.zoomIn(default)/.zoomOut/.flipUp/.flipDown
        presentPopUpViewController(pv)
    }
    

}
