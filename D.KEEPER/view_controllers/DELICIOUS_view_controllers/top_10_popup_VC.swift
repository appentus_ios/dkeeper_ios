//
//  top_10_popup_VC.swift
//  D.KEEPER
//
//  Created by appentus on 8/1/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit
import BubbleTransition

class top_10_popup_VC: UIViewController {

         @IBOutlet weak var scroll_view: UIScrollView!
         @IBOutlet weak var view_1: UIView!
         @IBOutlet weak var view_2: UIView!
         @IBOutlet weak var view_3: UIView!
         @IBOutlet weak var view_4: UIView!
         @IBOutlet weak var cancel_BTn: UIButton!
    
    
    weak var interactiveTransition: BubbleInteractiveTransition?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scroll_view.layer.cornerRadius = 4
        scroll_view.clipsToBounds = true
        
        
        border(view: view_1, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_1, color: .clear, corner_radius: view_1.frame.height/2)
        
        border(view: view_2, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_2, color: .clear, corner_radius: view_2.frame.height/2)
        border(view: view_3, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_3, color: .clear, corner_radius: view_3.frame.height/2)
        
        border(view: view_4, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_4, color: .clear, corner_radius: view_4.frame.height/2)

//        cancel_BTn.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)
        interactiveTransition?.finish()
        
    }
    
    @IBAction func cancel(_ sender: Any){
        
         dismissPopUpViewController()
        
    }
    
    deinit {
        print("deinit")
    }
    

}
