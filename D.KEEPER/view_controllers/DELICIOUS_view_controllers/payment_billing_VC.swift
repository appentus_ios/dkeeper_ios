//
//  payment_billing_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/25/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class payment_billing_VC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var view_1: UIView!
    @IBOutlet weak var view_2: UIView!
    @IBOutlet weak var view_3: UIView!
    @IBOutlet weak var view_4: UIView!
    @IBOutlet weak var view_5: UIView!
    

    @IBOutlet weak var card_num_TF: UITextField!
    @IBOutlet weak var card_validity_TF: UITextField!
    @IBOutlet weak var id_TF: UITextField!
    @IBOutlet weak var verification_code_TF: UITextField!
    @IBOutlet weak var card_code_TF: UITextField!
    
    var myPickerView : UIPickerView!
    var pickerData = ["Female" , "Male"]
    var card_picker = ["1","2","3","4","5","6","7","8","9","10","11","12"]
    var delegate: navigate_delegate?

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
           pickUp(card_code_TF)
        
        card_num_TF.setRightPaddingPoints(10)
        card_validity_TF.setRightPaddingPoints(10)
        id_TF.setRightPaddingPoints(10)
        verification_code_TF.setRightPaddingPoints(10)
        
        card_num_TF.setLeftPaddingPoints(10)
        card_validity_TF.setLeftPaddingPoints(10)
        id_TF.setLeftPaddingPoints(10)
        verification_code_TF.setLeftPaddingPoints(10)
        
        
        border(view: view_1, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_1, color: .clear, corner_radius: view_1.frame.height/2)
        
        border(view: view_2, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_2, color: .clear, corner_radius: view_2.frame.height/2)
        
        border(view: view_3, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_3, color: .clear, corner_radius: view_3.frame.height/2)
        
        border(view: view_4, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_4, color: .clear, corner_radius: view_4.frame.height/2)
        
        border(view: view_5, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_5, color: .clear, corner_radius: view_5.frame.height/2)
        
        // Do any additional setup after loading the view.
    }

    
    
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        textField.inputView = self.myPickerView
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return card_picker.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if card_code_TF.isFirstResponder == true {
            self.card_code_TF.text = card_picker[row]
            
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return card_picker[row]
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if card_code_TF.isFirstResponder == true {
            self.pickUp(card_code_TF)
        }
        
    }
    

    @IBAction func next_btn(_ sender: UIButton) {
        delegate?.nav_VC(tag: sender.tag)
    }
    
}
