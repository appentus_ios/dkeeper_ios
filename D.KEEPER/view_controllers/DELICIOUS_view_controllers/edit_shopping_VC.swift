//
//  edit_shopping_VC.swift
//  D.KEEPER
//
//  Created by appentus on 8/8/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class edit_shopping_VC: UIViewController {
    
      @IBOutlet weak var view_1:UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        view_1.layer.cornerRadius = view_1.frame.height/2
        // Do any additional setup after loading the view.
    }
    

    @IBAction func edit_btn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "shopping"), object:nil)

    }
    
    @IBAction func history_btn(_ sender: Any) {
        
    }
    
    
    @IBAction func ask(_ sender: Any) {
    }
    
    
    
    
}

extension edit_shopping_VC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "edit_shopping_tablecell", for: indexPath) as! edit_shopping_tablecell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
    
}
