//  general_explanation_VC.swift
//  D.KEEPER

//  Created by appentus on 7/23/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.


import UIKit

class general_explanation_VC: UIViewController {
    @IBOutlet weak var round_title_view:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        round_title_view.layer.cornerRadius = round_title_view.bounds.height/2
        round_title_view.clipsToBounds = true
    }
    

}
