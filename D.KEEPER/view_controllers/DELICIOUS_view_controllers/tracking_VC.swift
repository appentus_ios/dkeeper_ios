//  tracking_VC.swift
//  D.KEEPER

//  Created by appentus on 7/23/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.



import UIKit



class tracking_VC: UIViewController {
    @IBOutlet weak var view_1:UIView!
    @IBOutlet weak var view_2:UIView!
    @IBOutlet weak var view_3:UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shadow_on_view(view: view_3, color: .lightGray, corner_radius: 4)
        
        view_1.layer.cornerRadius = 4
        view_1.clipsToBounds = true
        
        view_2.layer.cornerRadius = 4
        view_2.clipsToBounds = true
    }
    
    @IBAction func talk(_ sender: Any) {
        
    }
    
    @IBAction func history(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "once_from_delicious")
        
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "week"), object:nil)
      
      
    }
    
    @IBAction func weight(_ sender: Any){
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "weight"), object:nil)
       }
    
}
