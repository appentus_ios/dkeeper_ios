//
//  request_restaurant_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/26/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class request_restaurant_VC: UIViewController {
    
    @IBOutlet weak var restaurant_TF: UITextField!
    @IBOutlet weak var city_TF: UITextField!
    @IBOutlet weak var view_1: UIView!

    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
          restaurant_TF.setRightPaddingPoints(15)
          city_TF.setRightPaddingPoints(15)
        
        
        border(view: restaurant_TF, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: restaurant_TF, color: .clear, corner_radius: restaurant_TF.frame.height/2)
        
        border(view: city_TF, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: city_TF, color: .clear, corner_radius: city_TF.frame.height/2)
        
        border(view: view_1, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_1, color: .clear, corner_radius: view_1.frame.height/2)
        

       
    }
    

    @IBAction func back(_ sender: Any){
        self.dismiss_VC()
    }
    
    @IBAction func send(_ sender: Any){
        
        }
    
    @IBAction func menu_btn(_ sender: Any){
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
}
