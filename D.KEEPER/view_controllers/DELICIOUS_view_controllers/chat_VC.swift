//
//  chat_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/25/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class chat_VC: UIViewController {

    @IBOutlet weak var chat_TF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        border(view: chat_TF, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: chat_TF, color: .clear, corner_radius: chat_TF.frame.height/2)

    chat_TF.setLeftPaddingPoints(25)
        chat_TF.setRightPaddingPoints(15)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func menu_btn(_ sender: Any){
         sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
    @IBAction func back(_ sender: Any){
        self.dismiss_VC()
    }
    

}

extension chat_VC: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row % 2 == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "chat_1_TableCell", for: indexPath) as! chat_1_TableCell
            return cell
        
        } else {
            let cell_1 = tableView.dequeueReusableCell(withIdentifier: "chat_2_TableCell", for: indexPath) as! chat_2_TableCell
            return cell_1
        
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    
    
    
    
    
}
