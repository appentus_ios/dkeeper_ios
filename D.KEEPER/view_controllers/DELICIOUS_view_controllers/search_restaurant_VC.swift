//
//  search_restaurant_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/26/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class search_restaurant_VC: UIViewController {
    @IBOutlet weak var view_1: UIView!
    
    @IBOutlet weak var view_2: UIView!
    
    @IBOutlet weak var view_3: UIView!
    
    @IBOutlet weak var view_4: UIView!
    
    @IBOutlet weak var restaurant_TF: UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()
        border(view: view_1, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_1, color: .clear, corner_radius: view_1.frame.height/2)
        
        border(view: view_2, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_2, color: .clear, corner_radius: view_2.frame.height/2)
        
        border(view: view_3, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_3, color: .clear, corner_radius: view_3.frame.height/2)
        
        border(view: view_4, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_4, color: .clear, corner_radius: view_4.frame.height/2)
        
        border(view: restaurant_TF, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: restaurant_TF, color: .clear, corner_radius: restaurant_TF.frame.height/2)
        

        sideMenuController?.rightViewPresentationStyle = .scaleFromBig
        sideMenuController?.rightViewStatusBarStyle = .lightContent
    
    }
    
    @IBAction func back(_ sender: Any){
       self.dismiss_VC()
    }
//
    @IBAction func menu_bar(_ sender: Any){
     sideMenuController?.showRightView(animated: true, completionHandler: nil)    }


}
