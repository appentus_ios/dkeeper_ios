//
//  info_preference_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/24/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class info_preference_VC: UIViewController {

    @IBOutlet weak var view_1:UIView!
    @IBOutlet weak var view_2:UIView!
    @IBOutlet weak var view_3:UIView!
    @IBOutlet weak var view_4:UIView!
    @IBOutlet weak var view_5:UIView!
    @IBOutlet weak var view_6:UIView!
    @IBOutlet weak var view_7:UIView!
    @IBOutlet weak var view_8:UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        border(view: view_1, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_1, color: .clear, corner_radius: view_1.frame.height/2)
        
        border(view: view_2, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_2, color: .clear, corner_radius: view_2.frame.height/2)
        
        border(view: view_3, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_3, color: .clear, corner_radius: view_3.frame.height/2)
        
        border(view: view_4, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_4, color: .clear, corner_radius: view_4.frame.height/2)
        
        border(view: view_5, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_5, color: .clear, corner_radius: view_5.frame.height/2)
        
        
        border(view: view_6, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_6, color: .clear, corner_radius: view_6.frame.height/2)
        
        border(view: view_7, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_7, color: .clear, corner_radius: view_7.frame.height/2)
        
        border(view: view_8, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_8, color: .clear, corner_radius: view_8.frame.height/2)
        
        
        // Do any additional setup after loading the view.
    }
    

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
