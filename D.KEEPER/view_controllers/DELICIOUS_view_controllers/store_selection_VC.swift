//
//  store_selection_VC.swift
//  D.KEEPER
//
//  Created by appentus on 9/12/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class store_selection_VC: UIViewController {

    var imag_arr = ["graphics_5", "graphics_7", "graphics_6", "graphics_4", "graphics_2", "graphics_3"]
    
    var selected_inde = 0
    var image_selected : UIImage?
    var title_selected: String?
    var header_selected: String?
    var detailed_selected: String?

    @IBOutlet weak var img_View: UIImageView!
    @IBOutlet weak var title_Lbl: UILabel!
    @IBOutlet weak var header_Lbl: UILabel!
    @IBOutlet weak var detailed_Lbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(selected_inde)
        img_View.image = image_selected
        title_Lbl.text = title_selected
        header_Lbl.text = header_selected
        detailed_Lbl.text = detailed_selected
    }
    
    @IBAction func back(_ sender: Any) {
        dismiss_VC()
    }
    
    @IBAction func side_menu_btn(_ sender: Any) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
    
    @IBAction func selection_btn(_ sender: Any) {
        if selected_inde == 2{
            push_VC(vc_name: "d_cook_container", storybord_name: "Main")
        } else {
        push_VC(vc_name: "purchase_plan_VC", storybord_name: "DELICIOUS")
}
    }

}
