//
//  purchase_billing_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/25/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit
import MRCountryPicker

class purchase_billing_VC: UIViewController , UITextFieldDelegate, MRCountryPickerDelegate{
    
    
   
    

    @IBOutlet weak var view_1: UIView!
    @IBOutlet weak var view_2: UIView!
    @IBOutlet weak var view_3: UIView!
    @IBOutlet weak var view_4: UIView!
    @IBOutlet weak var view_5: UIView!
    @IBOutlet weak var view_6: UIView!
    @IBOutlet weak var view_7: UIView!
    @IBOutlet weak var view_8: UIView!
    
    @IBOutlet weak var name_TF: UITextField!
    @IBOutlet weak var family_name_TF: UITextField!
    @IBOutlet weak var email_TF: UITextField!
    @IBOutlet weak var address_TF: UITextField!
    @IBOutlet weak var city_TF: UITextField!
    @IBOutlet weak var telephone_TF: UITextField!
    @IBOutlet weak var country_code_TF: UITextField!
    @IBOutlet weak var password_TF: UITextField!
    @IBOutlet weak var confirm_pass_TF: UITextField!

    var delegate: navigate_delegate?
    var myPickerView = MRCountryPicker()
   
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myPickerView.countryPickerDelegate = self
        myPickerView.showPhoneNumbers = true
        // set country by its name
        myPickerView.setCountryByName("India")
        pickUp(country_code_TF)
        name_TF.setRightPaddingPoints(10)
        family_name_TF.setRightPaddingPoints(10)
        email_TF.setRightPaddingPoints(10)
        address_TF.setRightPaddingPoints(10)
        city_TF.setRightPaddingPoints(10)
        telephone_TF.setRightPaddingPoints(10)
        password_TF.setRightPaddingPoints(10)
        confirm_pass_TF.setRightPaddingPoints(10)
        
        name_TF.setLeftPaddingPoints(10)
        family_name_TF.setLeftPaddingPoints(10)
        email_TF.setLeftPaddingPoints(10)
        address_TF.setLeftPaddingPoints(10)
        city_TF.setLeftPaddingPoints(10)
        telephone_TF.setLeftPaddingPoints(10)
        password_TF.setLeftPaddingPoints(10)
        confirm_pass_TF.setLeftPaddingPoints(10)
        
        
        
        border(view: view_1, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_1, color: .clear, corner_radius: view_1.frame.height/2)
        
        border(view: view_2, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_2, color: .clear, corner_radius: view_2.frame.height/2)
        
        border(view: view_3, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_3, color: .clear, corner_radius: view_3.frame.height/2)
        
        border(view: view_4, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_4, color: .clear, corner_radius: view_4.frame.height/2)
        
        border(view: view_5, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_5, color: .clear, corner_radius: view_5.frame.height/2)
        
        border(view: view_6, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_6, color: .clear, corner_radius: view_6.frame.height/2)
        
        border(view: view_7, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_7, color: .clear, corner_radius: view_7.frame.height/2)
        
        border(view: view_8, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_8, color: .clear, corner_radius: view_8.frame.height/2)
        
       
    }
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        country_code_TF.text = phoneCode
    }
    func pickUp(_ textField : UITextField){
        self.myPickerView.backgroundColor = UIColor.white
        textField.inputView = self.myPickerView
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if country_code_TF.isFirstResponder == true {
             self.pickUp(country_code_TF)
        }
    }
    
    @IBAction func next_Step_Btn(_ sender: UIButton) {
        delegate?.nav_VC(tag: sender.tag)
    }

}
