//
//  purchase_plan_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/25/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class purchase_plan_VC: UIViewController, navigate_delegate {
  
    

    @IBOutlet weak var menu_view: MenuTabsView!
    
    var once = false
    var curr = Int()
    var currentIndex: Int = 0
    var tabs = ["הצהרת בריאו"
        ,
                "פרטי חיוב"
        ,
    "שיטת תשלום"
]
    var pageController: UIPageViewController!
    
    var arrVC = [UIViewController]()
    
    
        override func viewDidLoad() {
        super.viewDidLoad()
        
            
            curr = 0
            let controller_1  = storyboard?.instantiateViewController(withIdentifier: "health_statement_VC") as! health_statement_VC
            let controller_2  = storyboard?.instantiateViewController(withIdentifier: "purchase_billing_VC") as! purchase_billing_VC
            let controller_3 =  storyboard?.instantiateViewController(withIdentifier: "payment_billing_VC") as! payment_billing_VC
            let controller_4  = storyboard?.instantiateViewController(withIdentifier: "order_billing_VC") as! order_billing_VC
            let controller_5 = storyboard?.instantiateViewController(withIdentifier: "confirm_program_VC") as! confirm_program_VC
         
              arrVC = [controller_1,controller_2,controller_3,controller_4, controller_5]
            
            controller_1.delegate = self
            controller_2.delegate = self
            controller_3.delegate = self
            controller_4.delegate = self
     
            
            menu_view.dataArray = tabs
            menu_view.isSizeToFitCellsNeeded = true
            menu_view.collView.backgroundColor = .clear
            presentPageVCOnView()
            
            menu_view.menuDelegate = self
            pageController.delegate = self
            pageController.dataSource = self
            
            //For Intial Display
            menu_view.collView.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: [], animated: true)
//          menu_view.collView.selectItem(at: IndexPath.init(item: 0, section: 0), animated: true, scrollPosition: .right)
            pageController.setViewControllers([arrVC[0]], direction: .reverse, animated: true, completion: nil)
            pageController.view.semanticContentAttribute = .forceRightToLeft
        sideMenuController?.rightViewPresentationStyle = .scaleFromBig
        sideMenuController?.rightViewStatusBarStyle = .lightContent

            
            
            let current_index = 0
            
            for i in 0..<color_arr.count{
                if i == current_index{
                    color_arr[i] = "#EB92A1"
                }else{
                    color_arr[i] = "#8895A3"
                }
            }
            for i in 0..<color_arr_2.count{
                if i == current_index{
                    arrrr[i] =  hexStringToUIColor(hex: "#EB92A1")
                }else{
                    arrrr[i] = UIColor.clear
                }
            }
            
            
            menu_view.collView.reloadData()
            menu_view.collView.selectItem(at: IndexPath.init(item: current_index, section: 0), animated: true, scrollPosition: .centeredVertically)
            menu_view.collView.scrollToItem(at: IndexPath.init(item: current_index, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    func presentPageVCOnView() {
        
        self.pageController = storyboard?.instantiateViewController(withIdentifier: "purchase_pageVC") as! purchase_pageVC
        self.pageController.view.frame = CGRect.init(x: 0, y: menu_view.frame.maxY, width: view.frame.width, height: view.frame.height - menu_view.frame.maxY)
        self.addChild(self.pageController)
        self.view.addSubview(self.pageController.view)
        self.pageController.didMove(toParent: self)
        
    }
    
    func viewController(At index: Int) -> UIViewController? {
        
        if((self.menu_view.dataArray.count == 1) || (index >= self.menu_view.dataArray.count)) {
            return nil
        } else {
            let vc = self.arrVC[index]
            currentIndex = index
            return vc
        }
    }
    
    @IBAction func menu_btn(_ sender: Any) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    @IBAction func back(_ sender: Any) {
        dismiss_VC()
    }
    
    func nav_VC(tag: Int) {
        for i in 0..<arrVC.count{
            if i == tag{
                self.pageController.setViewControllers([arrVC[i]], direction: .reverse, animated: true, completion: nil)
                if i == 4 {
                    menu_view.collView.scrollToItem(at: IndexPath.init(item: 2, section: 0), at: .centeredHorizontally, animated: true)
                } else if i == 3 {
                menu_view.collView.scrollToItem(at: IndexPath.init(item: 2, section: 0), at: .centeredHorizontally, animated: true)
                
                } else {
                     menu_view.collView.scrollToItem(at: IndexPath.init(item: tag, section: 0), at: .centeredHorizontally, animated: true)
                }
                
                color_arr[i]  = "#EB92A1"
                arrrr[i] =  hexStringToUIColor(hex: "#EB92A1")
                   
            } else {
                color_arr[i] = "#8895A3"
                arrrr[i] = UIColor.clear
            }
        }
        menu_view.collView.reloadData()
    
    }
    
//    func nav_VC2() {
//    
//        self.pageController.setViewControllers([arrVC[4]], direction: .reverse, animated: true, completion: nil)
//        menu_view.collView.scrollToItem(at: IndexPath.init(item: 3, section: 0), at: .centeredHorizontally, animated: true)
//    }
//    
    
}


extension purchase_plan_VC: MenuBarDelegate {
    
    func menuBarDidSelectItemAt(menu: MenuTabsView, index: Int) {
        
        if index != currentIndex {
            
            if index > currentIndex {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .forward, animated: true, completion: nil)
            }else {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .reverse, animated: true, completion: nil)
            }
            for i in 0..<color_arr.count{
                if i == index{
                    color_arr[i] = "#EB92A1"
                }else{
                    color_arr[i] = "#8895A3"
                }
            }
            
            for i in 0..<color_arr_2.count{
                if i == index{
                    arrrr[i] =  hexStringToUIColor(hex: "#EB92A1")
                }else{
                    arrrr[i] = UIColor.clear
                }
            }
            menu_view.collView.reloadData()
            menu_view.collView.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .centeredHorizontally, animated: true)
            
        }
        
    }
    
}


extension purchase_plan_VC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController)-> UIViewController? {
        curr = arrVC.firstIndex(of: viewController)!
        // if you prefer to NOT scroll circularly, simply add here:
        if curr == 0 { return nil }
        let prev = abs((curr - 1) % arrVC.count)
        
        return arrVC[prev]
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController)-> UIViewController? {
        curr = arrVC.firstIndex(of: viewController)!
        // if you prefer to NOT scroll circularly, simply add here:
        if curr == (arrVC.count - 1) { return nil }
        let nxt = abs((curr + 1) % arrVC.count)
        
        return arrVC[nxt]
    }
    
    func nextPageWithIndex(index: Int)
    {
        // let nextWalkthroughVC = newColoredViewController("setup4")
        let nextWalkthroughVC = arrVC[index]
        self.pageController.setViewControllers([nextWalkthroughVC], direction: .forward, animated: true, completion: nil)
        
    }
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if finished {
            if completed {
                
                
                let current_index = arrVC.firstIndex(of: pageViewController.viewControllers!.first!)
                
                if current_index == 4 || current_index == 3 {
                    menu_view.collView.scrollToItem(at: IndexPath.init(item: 2, section: 0), at: .centeredHorizontally, animated: true)
//                    menu_view.collView.selectItem(at: IndexPath.init(item: 3, section: 0), animated: true, scrollPosition: .centeredVertically)
                   
                } else {
                    menu_view.collView.scrollToItem(at: IndexPath.init(item: current_index!, section: 0), at: .centeredHorizontally, animated: true)
                     menu_view.collView.selectItem(at: IndexPath.init(item: current_index!, section: 0), animated: true, scrollPosition: .centeredHorizontally)
                    
                }

                
                for i in 0..<color_arr.count{
                    if i == current_index! {
                        color_arr[i] = "#EB92A1"
                        
                    }else{
                        color_arr[i] = "#8895A3"
                    }
                }
                for i in 0..<color_arr_2.count{
                    if i == current_index!{
                        arrrr[i] =  hexStringToUIColor(hex: "#EB92A1")
                    }else{
                        arrrr[i] = UIColor.clear
                    }
                }
                
                
                
                 menu_view.collView.reloadData()
                
              
//                menu_view.collView.scrollToItem(at: IndexPath.init(item: current_index!, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
    }
}




