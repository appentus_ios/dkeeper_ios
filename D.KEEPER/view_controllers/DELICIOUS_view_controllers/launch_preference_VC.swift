//
//  launch_preference_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/30/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class launch_preference_VC: UIViewController {
   
    
    
    @IBOutlet weak var pop_view: UIView!
    @IBOutlet weak var protine_view: UIView!
    @IBOutlet weak var carbohydrate_view: UIView!
    @IBOutlet weak var extra_view: UIView!
    @IBOutlet weak var scroll_view: UIScrollView!
    
  

    

    

    override func viewDidLoad() {
        super.viewDidLoad()

        scroll_view.layer.cornerRadius = 4
        scroll_view.clipsToBounds = true
        
        border(view: protine_view, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: protine_view, color: .clear, corner_radius: protine_view.frame.height/2)
        border(view: carbohydrate_view, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: carbohydrate_view, color: .clear, corner_radius: carbohydrate_view.frame.height/2)
        border(view: extra_view, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: extra_view, color: .clear, corner_radius: extra_view.frame.height/2)
  
    }
    
    
   
    
    
    @IBAction func pop_dismiss(_ sender: Any) {
        
    }
    
   
}
