//
//  order_billing_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/25/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class order_billing_VC: UIViewController {

    @IBOutlet weak var view_1: UIView!
    @IBOutlet weak var view_2: UIView!
    @IBOutlet weak var view_3: UIView!
    @IBOutlet weak var view_4: UIView!
    @IBOutlet weak var view_5: UIView!
    @IBOutlet weak var view_6: UIView!
    
    @IBOutlet weak var  newletter_Btn: UIButton!
    @IBOutlet weak var application_Btn: UIButton!
    
     var delegate: navigate_delegate?
   
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        border(view: view_1, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_1, color: .clear, corner_radius: view_1.frame.height/2)
        
        border(view: view_2, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_2, color: .clear, corner_radius: view_2.frame.height/2)
        
        border(view: view_3, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_3, color: .clear, corner_radius: view_3.frame.height/2)
        
        border(view: view_4, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_4, color: .clear, corner_radius: view_4.frame.height/2)
        
        border(view: view_5, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_5, color: .clear, corner_radius: view_5.frame.height/2)
        
        border(view: view_6, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: view_6, color: .clear, corner_radius: view_6.frame.height/2)
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func selection(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.tag == 0{
            if (sender.isSelected == true) {
                application_Btn.setImage(UIImage(named: "Group 3360-1"), for: .normal)
                
            } else{
                application_Btn.setImage(UIImage(named: "Ellipse 343"), for: .selected)
            }
            
        } else {
            if (sender.isSelected == true) {
                newletter_Btn.setImage(UIImage(named: "Group 3360-1"), for: .normal)
            } else{
                newletter_Btn.setImage(UIImage(named: "Ellipse 343"), for: .selected)
            }
            
        }
        
    }
    
    @IBAction func reservation_Btn(_ sender: UIButton) {
        delegate?.nav_VC(tag: sender.tag)
     
    }
    
}
