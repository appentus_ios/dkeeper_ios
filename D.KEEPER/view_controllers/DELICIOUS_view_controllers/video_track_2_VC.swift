//
//  video_track_2_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/25/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class video_track_2_VC: UIViewController {
    
    @IBOutlet weak var view_1:UIView!
    @IBOutlet weak var view_2:UIView!
    @IBOutlet weak var view_3:UIView!
    @IBOutlet weak var view_4:UIView!
    @IBOutlet weak var view_5:UIView!
    @IBOutlet weak var view_6:UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        shadow_on_view(view: view_1, color: .clear, corner_radius: view_1.frame.height/2)
        shadow_on_view(view: view_2, color: .clear, corner_radius: view_2.frame.height/2)
        shadow_on_view(view: view_3, color: .clear, corner_radius: view_3.frame.height/2)
        shadow_on_view(view: view_4, color: .clear, corner_radius: view_4.frame.height/2)
        shadow_on_view(view: view_5, color: .clear, corner_radius: view_5.frame.height/2)
        shadow_on_view(view: view_6, color: .clear, corner_radius: view_6.frame.height/2)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn(_ sender: Any){
        
         NotificationCenter.default.post(name: NSNotification.Name (rawValue: "video"), object:nil)
        
    }

}
