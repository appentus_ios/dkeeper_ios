//
//  top_10_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/26/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit


class top_10_VC: UIViewController {

        override func viewDidLoad() {
        super.viewDidLoad()
        }
    
    
    

    @IBAction func filter_btn(_ sender: Any) {
        
        let s                   = UIScreen.main.bounds.size
        let pv                  = UIStoryboard(name: "DELICIOUS", bundle: nil).instantiateViewController(withIdentifier: "top_10_popup_VC") as! top_10_popup_VC
        pv.view.frame           = CGRect(x:0, y:0, width: view.frame.width, height: view.frame.height)
        pv.view.backgroundColor = .clear
        popUpEffectType         = .zoomOut //.zoomIn(default)/.zoomOut/.flipUp/.flipDown
        presentPopUpViewController(pv)
    }
    
    @IBAction func back_btn(_ sender: Any) {
        dismiss_VC()
    }
    @IBAction func menu_btn(_ sender: Any) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    @IBAction func restaurant_details(_ sender: UIButton){
        push_VC(vc_name: "recommended_VC", storybord_name: "DELICIOUS")
    }
    
}

extension top_10_VC:  UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "top_10_table_cell", for: indexPath) as! top_10_table_cell
        cell.detail_btn.addTarget(self, action: #selector(restaurant_details(_:)), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
}
