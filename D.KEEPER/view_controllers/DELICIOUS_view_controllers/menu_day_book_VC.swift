//
//  menu_day_book_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/24/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class menu_day_book_VC: UIViewController {
    
    @IBOutlet weak var table_1: UITableView!
    @IBOutlet weak var table_2: UITableView!



    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func send_staff_btn(_ sender: Any) {
        
    }
    

    @IBAction func history_btn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "menu_eating"), object:nil)

        
    }
    


    @IBAction func ask_btn(_ sender: Any) {
//        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "chat"), object:nil)

        
    }
    


}

extension menu_day_book_VC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == table_1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "menu_day_book_1_TableCell", for: indexPath) as! menu_day_book_1_TableCell
            
            
            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "menu_day_book_2_TableCell", for: indexPath) as! menu_day_book_2_TableCell
            
            if indexPath.row % 2 == 0 {
                cell.view_2.backgroundColor = UIColor.init(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1)
                cell.time_Lbl.textColor = UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1)
                cell.wake_lbl.textColor = UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1)
                cell.detail_lbl.textColor = UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1)
//                cell.edit_imgView.setBackgroundImage(UIImage(named: "switch_button-1"), for: .normal)
                cell.edit_imgView.setImage(UIImage(named: "switch_button"), for: .normal)
//                cell.swipe_Img.setBackgroundImage(UIImage(named: "EDITBUTTON"), for: .normal)
                
                cell.swipe_Img.setImage(UIImage(named: "edit_button-2"), for: .normal)
                cell.bar_lbl.backgroundColor =  UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1)
                
                
                
            }else{
                cell.view_2.backgroundColor = UIColor.init(red: 41.0/255.0, green: 67.0/255.0, blue: 96.0/255.0, alpha: 1)
                cell.time_Lbl.textColor = UIColor.white
                cell.wake_lbl.textColor = UIColor.white
                cell.detail_lbl.textColor = UIColor.white
//                cell.edit_imgView.setBackgroundImage(UIImage(named: "edit_button-2"), for: .normal)
                 cell.edit_imgView.setImage(UIImage(named: "switch_button-1"), for: .normal)
//                cell.swipe_Img.setBackgroundImage(UIImage(named: "switch_button"), for: .normal)
                
                cell.swipe_Img.setImage(UIImage(named: "EDITBUTTON"), for: .normal)
                
                cell.bar_lbl.backgroundColor = UIColor.white
                
            }
            
            
            
            return cell
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == table_1{
            return 90
        } else {
            return 205
        }
    }
    
}
