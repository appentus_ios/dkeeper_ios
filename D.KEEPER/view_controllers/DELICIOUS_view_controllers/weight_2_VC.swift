//
//  weight_2_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/25/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class weight_2_VC: UIViewController {

    
    @IBOutlet weak var view_1:UIView!
    @IBOutlet weak var img_view:UIImageView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
      
         view_1.layer.cornerRadius = 4
        view_1.clipsToBounds = true
    }
    

    @IBAction func plus_btn(_ sender: UIButton){
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "weight"), object:nil)

        
    }
    
    @IBAction func history_btn(_ sender: UIButton){
        NotificationCenter.default.post(name: NSNotification.Name (rawValue: "week"),  object: nil)
    }

}
