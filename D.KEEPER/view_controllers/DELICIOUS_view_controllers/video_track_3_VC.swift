//
//  video_track_3_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/25/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class video_track_3_VC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        
    }
    
    @IBAction func playvideo(_ sender: UIButton){
         NotificationCenter.default.post(name: NSNotification.Name (rawValue: "video_play"), object:nil)
        
    }
    
    


}


extension video_track_3_VC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "video_track_3_TableCell", for: indexPath) as! video_track_3_TableCell
        
        cell.btn.addTarget(self, action: #selector(playvideo(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    

    
}
