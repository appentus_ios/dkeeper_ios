//
//  calendar_popup_VC.swift
//  D.KEEPER
//
//  Created by appentus on 8/5/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit
import VACalendar


class calendar_popup_VC: UIViewController , UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    let defaultCalendar: Calendar = {
        var calendar = Calendar.current
        calendar.firstWeekday = 1
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar
    }()
    
    
    var myPickerView : UIPickerView!
    var datepicker: UIDatePicker!
    var pickerData = ["דצמבר","נובמב","אוקטוב","ספטמב","אוגוסט","יולי","יוני","מאי","אפריל","מרץ","פברואר","ינואר"]
 

//
//    @IBOutlet weak var monthHeaderView: VAMonthHeaderView! {
//        didSet {
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy LLLL"
//            let appereance = VAMonthHeaderViewAppearance(
//                previousButtonImage: UIImage(named: "Group 120")!,
//                nextButtonImage: UIImage(named: "Group 120-1")!,
//                dateFormatter: dateFormatter
//            )
//            monthHeaderView.delegate = self
//            monthHeaderView.appearance = appereance
//        }
//    }
    
    
    
 
    @IBOutlet weak var clendar_view_number: UIView!
    @IBOutlet weak var pop_view: UIView!

    @IBOutlet weak var year_picker: UITextField!
    @IBOutlet weak var month_picker: UITextField!

    
    
    var calendarView: VACalendarView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickUp(month_picker)
        pickUp(year_picker)
        
        pop_view.layer.cornerRadius = 4
        pop_view.clipsToBounds = true
        let calendar = VACalendar(calendar: defaultCalendar)
        calendarView = VACalendarView(frame: .zero, calendar: calendar)
        calendarView.showDaysOut = true
        calendarView.backgroundColor = UIColor.white
        calendarView.selectionStyle = .single
//      calendarView.monthDelegate = monthHeaderView
        calendarView.dayViewAppearanceDelegate = self
        calendarView.monthViewAppearanceDelegate = self
        calendarView.calendarDelegate = self
        calendarView.scrollDirection = .horizontal

        clendar_view_number.addSubview(calendarView)

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if calendarView.frame == .zero {
            calendarView.frame = CGRect(
                x: 0,
                y: 20,
                width: clendar_view_number.frame.width,
                height: clendar_view_number.frame.height
            )
            calendarView.setup()
        }
    }
    
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        textField.inputView = self.myPickerView
    }

    
    @objc func doneClick() {
        year_picker.resignFirstResponder()
        month_picker.resignFirstResponder()
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if month_picker.isFirstResponder == true {
            self.month_picker.text = pickerData[row]
            
        } else {
            self.year_picker.text = pickerData[row]
            
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if month_picker.isFirstResponder == true {
            return pickerData[row]
        } else {
            return pickerData[row]
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if month_picker.isFirstResponder == true {
            self.pickUp(month_picker)
            
        } else {
            self.pickUp(year_picker)
            
        }
         
        
    }
    
    @IBAction func cancel(_ sender: Any){
        
        dismissPopUpViewController()
        
    }
    
    deinit {
        print("deinit")
    }
    

}

extension calendar_popup_VC: VAMonthHeaderViewDelegate {
    
    func didTapNextMonth() {
        calendarView.nextMonth()
    }
    
    func didTapPreviousMonth() {
        calendarView.previousMonth()
    }
    
}

extension calendar_popup_VC: VAMonthViewAppearanceDelegate {
    
    func leftInset() -> CGFloat {
        return 10.0
    }
    
    func rightInset() -> CGFloat {
        return 10.0
    }
    
    func verticalMonthTitleFont() -> UIFont {
        return UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
    
    func verticalMonthTitleColor() -> UIColor {
        return .black
    }
    
    func verticalCurrentMonthTitleColor() -> UIColor {
        return .red
    }
    
}

extension calendar_popup_VC: VADayViewAppearanceDelegate {
    
    func textColor(for state: VADayState) -> UIColor {
        switch state {
        case .out:
            return hexStringToUIColor(hex: "#3CFDBE")
//                UIColor(red: 214 / 255, green: 214 / 255, blue: 219 / 255, alpha: 1.0)
        case .selected:
            return .white
        case .unavailable:
            return UIColor.darkGray
        default:
            return .black
        }
    }
    
    func textBackgroundColor(for state: VADayState) -> UIColor {
        switch state {
        case .selected:
            return hexStringToUIColor(hex: "#3CFDBE")
        default:
            return .clear
        }
    }
    
    func shape() -> VADayShape {
        return .circle
    }
    
    func dotBottomVerticalOffset(for state: VADayState) -> CGFloat {
        switch state {
        case .selected:
            return 2
        default:
            return -7
        }
    }
    
}

extension calendar_popup_VC: VACalendarViewDelegate {
    
    func selectedDates(_ dates: [Date]) {
        calendarView.startDate = dates.last ?? Date()
        print(dates)
    }
    
}

