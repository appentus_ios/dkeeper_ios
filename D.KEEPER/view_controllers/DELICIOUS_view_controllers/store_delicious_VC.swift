//
//  store_delicious_VC.swift
//  D.KEEPER
//
//  Created by appentus on 9/12/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class store_delicious_VC: UIViewController {

    @IBOutlet weak var coll_View: UICollectionView!
    var imag_arr = ["graphics_5", "graphics_7", "graphics_6", "graphics_4", "graphics_2", "graphics_3"]
    var title_arr = [
        "D.ELICIOUS + D.FIT | 30 ש \"ח",
         "D.COUPLES | 30 ש\"ח",
        "D.COOK |  30 ש\"ח",
        "D.ELICIOUS + D.FIT | 30 ש \"ח",
       "D.FIT |  30 ש\"ח",
        "D.ELICIOUS |  30 ש\"ח",
    ]
    
    var detailed_arr = [                        "תוכנית מלאה למה לא גם וגם? רוצים ללמוד לאכול נכון, לרדת במשקל ותוך כדי להתחיל להתאמן סוף סוף? קבלו את הערכה המשולבת הכוללת גם את מדריך האימונים וגם את מדריך התזונה- והכל? בשביל להשיג מקסימום תוצאות במינימום זמן.י",
                                                "תוכנית מלאה למה לא גם וגם? רוצים ללמוד לאכול נכון, לרדת במשקל ותוך כדי להתחיל להתאמן סוף סוף? קבלו את הערכה המשולבת הכוללת גם את מדריך האימונים וגם את מדריך התזונה- והכל? בשביל להשיג מקסימום תוצאות במינימום זמן.י",

                                                "תוכנית בישול למה לא גם וגם? רוצים ללמוד לאכול נכון, לרדת במשקל ותוך כדי להתחיל להתאמן סוף סוף? קבלו את הערכה המשולבת הכוללת גם את מדריך האימונים וגם את מדריך התזונה- והכל? בשביל להשיג מקסימום תוצאות במינימום זמן.י",
                                                "תוכנית מלאה למה לא גם וגם? רוצים ללמוד לאכול נכון, לרדת במשקל ותוך כדי להתחיל להתאמן סוף סוף? קבלו את הערכה המשולבת הכוללת גם את מדריך האימונים וגם את מדריך התזונה- והכל? בשביל להשיג מקסימום תוצאות במינימום זמן.י",

        " תוכנית אימונים ערכת אימונים הכוללת מדריך אימונים של 12 שבועות. בערכה תוכלו למצוא מעל 36 אימונים שונים שיסייעו לכם בחיטוב הגוף, בשריפת שומן ובשיפור סיבולת לב ריאה. תוכלו להתאמן בכל מקום, בכל זמן וללא כל צורך בחדר כושר. י",
                        "תוכנית מלאה למה לא גם וגם? רוצים ללמוד לאכול נכון, לרדת במשקל ותוך כדי להתחיל להתאמן סוף סוף? קבלו את הערכה המשולבת הכוללת גם את מדריך האימונים וגם את מדריך התזונה- והכל? בשביל להשיג מקסימום תוצאות במינימום זמן.י ",
    ]
    override func viewDidLoad() {
        super.viewDidLoad()

        coll_View.delegate = self
        coll_View.dataSource = self
        
    }
    @objc func selected(_ sender: UIButton){
        let storyboard = UIStoryboard(name: "DELICIOUS", bundle: nil)
        let nav = storyboard.instantiateViewController(withIdentifier: "store_selection_VC") as! store_selection_VC
//        nav.img_View.image = UIImage(named: imag_arr[sender.tag])
        nav.selected_inde = sender.tag
        nav.image_selected = UIImage(named: imag_arr[sender.tag])
        nav.title_selected = title_arr[sender.tag]
        nav.detailed_selected = detailed_arr[sender.tag]
        self.navigationController?.pushViewController(nav, animated: true)
       
    }
    

    @IBAction func back(_ sender: Any) {
        dismiss_VC()
    }
    
    @IBAction func side_menu(_ sender: Any) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
}

extension store_delicious_VC: UICollectionViewDelegate{
    
}

extension store_delicious_VC: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "store_delicious_CollectionViewCell", for: indexPath) as! store_delicious_CollectionViewCell
        cell.selection_btn.tag = indexPath.row
        cell.image_View.image = UIImage(named: imag_arr[indexPath.row])
        cell.title_lbl.text = title_arr[indexPath.row]
        cell.selection_btn.addTarget(self, action: #selector(selected(_:)), for: .touchUpInside)
        
        return cell
    }
    
    
}

extension store_delicious_VC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width/2 , height: 235)
    }
}
