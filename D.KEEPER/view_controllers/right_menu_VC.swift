//
//  right_menu_VC.swift
//  D.KEEPER
//
//  Created by Rajat Pathak on 11/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class right_menu_VC: UIViewController {
    let arr_menu_icon = ["home","ring","woman","magnifying-glass","ribbon","open-book","edit","question","shopping-cart (1)","feather-pen","controls","contact"]
    var arr_menu_title = ["יצירת קשר"
        , "הגדרות"
        , "טיפים"
        , "חנות"
        , "צ'אט"
        , "שאלות נפוצות"
        , "מתכונים"
        , "TOP 10 מסעדות"
        , "חיפוש מסעדה"
        , "פרופיל"
        , "עדכונים"
        , "עמוד הבית"
    ]
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arr_menu_title.reverse()
    }
    
    @IBAction func menu_btn(_ sender: UIButton) {
        sideMenuController?.hideRightView()
        NotificationCenter.default.post(name: NSNotification.Name("nav_main"), object: nil,userInfo: ["sender":0])
    }
}



extension right_menu_VC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_menu_icon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! right_menu_TableViewCell
        
        cell.menu_title_lbl.text = arr_menu_title[indexPath.row]
        cell.menu_icon_img.image = UIImage (named:arr_menu_icon[indexPath.row])
        
        cell.select_btn.tag = indexPath.row
        cell.select_btn.addTarget(self, action: #selector(select_btn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func select_btn(_ sender:UIButton) {
        sideMenuController?.hideRightView()
//        self.btn_right_menu(sender)
        NotificationCenter.default.post(name: NSNotification.Name("nav_main"), object: nil,userInfo: ["sender":sender.tag])
    }
    
   
}
