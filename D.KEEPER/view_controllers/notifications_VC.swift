//
//  notifications_VC.swift
//  D.KEEPER
//
//  Created by ayush pathak on 15/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class notifications_VC: UIViewController , UITableViewDelegate , UITableViewDataSource {
    @IBOutlet weak var table_view: UITableView!
    
    var title_array = ["אלחנדרוס, מסעדה חדשה","מתכון חדש, פשוט וטעים לדג על ","אלחנדרוס, מסעדה חדשה","מתכון חדש, פשוט וטעים לדג על ","אלחנדרוס, מסעדה חדשה","מתכון חדש, פשוט וטעים לדג על ","אלחנדרוס, מסעדה חדשה","מתכון חדש, פשוט וטעים לדג על ",]
    
    var desc_array = ["ודי.קיפרית בתל אביב בקינג-ג'ורג' ממש מעבר לפינה של דיזינגוף סנטר נפתחה מסעדה שעוזרת לשמור על הדיאטה :) י",
                                "מתכון חדש, פשוט וטעים לדג על האש! י לבשל דגים לא חייב להיות פרויקט, אם יודעים איך לעשות את זה קצר וקולע. י"
        ,"ודי.קיפרית בתל אביב בקינג-ג'ורג' ממש מעבר לפינה של דיזינגוף סנטר נפתחה מסעדה שעוזרת לשמור על הדיאטה :) י",
         "מתכון חדש, פשוט וטעים לדג על האש! י לבשל דגים לא חייב להיות פרויקט, אם יודעים איך לעשות את זה קצר וקולע. י"
        ,"ודי.קיפרית בתל אביב בקינג-ג'ורג' ממש מעבר לפינה של דיזינגוף סנטר נפתחה מסעדה שעוזרת לשמור על הדיאטה :) י",
         "מתכון חדש, פשוט וטעים לדג על האש! י לבשל דגים לא חייב להיות פרויקט, אם יודעים איך לעשות את זה קצר וקולע. י"
        ,"ודי.קיפרית בתל אביב בקינג-ג'ורג' ממש מעבר לפינה של דיזינגוף סנטר נפתחה מסעדה שעוזרת לשמור על הדיאטה :) י",
         "מתכון חדש, פשוט וטעים לדג על האש! י לבשל דגים לא חייב להיות פרויקט, אם יודעים איך לעשות את זה קצר וקולע. י"
        ,]
    
    
    
    var img_icons = ["notification_1","botification_2","notification_3",
                    "notification_1","botification_2","notification_3",
                    "notification_1","botification_2","notification_3",
                    "notification_1","botification_2","notification_3",
                    "notification_1","botification_2","notification_3",
                    "notification_1","botification_2","notification_3",
                    "notification_1","botification_2","notification_3",]
    
    
    var details_array = ["למתכון","למנות המומלצות","למתכון","למתכון","למנות המומלצות","למתכון","למתכון","למנות המומלצות","למתכון"
    ,"למתכון","למנות המומלצות","למתכון"
    ,"למתכון","למנות המומלצות","למתכון"
    ,"למתכון","למנות המומלצות","למתכון"
    ,"למתכון","למנות המומלצות","למתכון"
    ,"למתכון","למנות המומלצות","למתכון"
    ,"למתכון","למנות המומלצות","למתכון"
            ]
    
    var is_current_VC = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideMenuController?.rightViewPresentationStyle = .scaleFromBig
        sideMenuController?.rightViewStatusBarStyle = .lightContent
        
        NotificationCenter.default.addObserver(self, selector: #selector(func_push(notification:)), name: NSNotification.Name (rawValue: "push_notification"), object: nil)
    }
    
    @objc func func_push(notification:Notification) {
//        if !is_current_VC {
            is_current_VC = !is_current_VC
            push_VC(vc_name:"\(notification.userInfo!["identifier"]!)", storybord_name: "Main")
//        }
    }
    
    @IBAction func menu_btn(_ sender: Any) {
        sideMenuController?.showRightView(animated: true, completionHandler: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return title_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notifications_cells") as! notifications_cells
        
        cell.lbl_title.text = title_array[indexPath.row]
        cell.lbl_desc.text = desc_array[indexPath.row]
        
        cell.img_icon.image = UIImage (named:img_icons[indexPath.row])
        
        cell.width_details.constant = rectForText(text: details_array[indexPath.row])+40
        cell.details_btn.setTitle(details_array[indexPath.row], for: .normal)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    @IBAction func back(_ sender: UIButton) {
        dismiss_VC()
    }
    
    
}




func rectForText(text: String) -> CGFloat {
    let maxSize = CGSize (width: 1000, height: 100)
    let font = UIFont (name: "Assistant-Regular", size:12.0)
    let attrString = NSAttributedString.init(string: text, attributes: [NSAttributedString.Key.font:font!])
    let rect = attrString.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
    
    return rect.size.width
}





