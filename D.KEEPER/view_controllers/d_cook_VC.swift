//
//  d_cook_VC.swift
//  D.KEEPER
//
//  Created by appentus on 7/19/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class d_cook_VC: UIViewController {

    var delegte_call: navigate_delegate?
    
    @IBOutlet weak var first_view: UIView!
    @IBOutlet weak var daily_routine_Lbl: UILabel!
    
    @IBOutlet weak var second_view: UIView!
    @IBOutlet weak var office_route_Lbl: UILabel!
    
    @IBOutlet weak var third_view: UIView!
    @IBOutlet weak var mini_office_route_Lbl: UILabel!
    
    @IBOutlet weak var fourth_view: UIView!
    @IBOutlet weak var weekly_route_Lbl: UILabel!
    
    @IBOutlet weak var fivth_view: UIView!
    @IBOutlet weak var track_lbl: UILabel!
    
    @IBOutlet weak var check_1: UIButton!
    @IBOutlet weak var check_2: UIButton!
    @IBOutlet weak var check_3: UIButton!
    @IBOutlet weak var check_4: UIButton!
    @IBOutlet weak var check_5: UIButton!
    
    
    @IBOutlet weak var info_1: UIButton!
    @IBOutlet weak var info_2: UIButton!
    @IBOutlet weak var info_3: UIButton!
    @IBOutlet weak var info_4: UIButton!
    @IBOutlet weak var ingo_5: UIButton!
    
    
    var arr = [false,false,false,false,false]
    
    var titlee =    [UILabel]()
    var back_view = [UIView]()
    var info_btn =  [UIButton]()
    var check_btn = [UIButton]()

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titlee =     [daily_routine_Lbl,office_route_Lbl,mini_office_route_Lbl,weekly_route_Lbl,track_lbl]
        back_view = [first_view,second_view,third_view,fourth_view,fivth_view]
        info_btn =  [info_1,info_2,info_3,info_4,ingo_5]
        check_btn = [check_1,check_2,check_3,check_4,check_5]
    

        shadow_on_view(view: first_view, color: .clear, corner_radius: first_view.frame.height/2)
        shadow_on_view(view: second_view , color: .clear, corner_radius: second_view.frame.height/2)
        shadow_on_view(view: third_view, color: .clear, corner_radius: third_view.frame.height/2)
        shadow_on_view(view: fourth_view, color: .clear, corner_radius: fourth_view.frame.height/2)
        shadow_on_view(view: fivth_view, color: .clear, corner_radius: fivth_view.frame.height/2)
        // Do any additional setup after loading the view.

    }

    @IBAction func back(_ sender: Any) {
        
        dismiss_VC()
    }
    
    
    func tag_wise_selection(tag : Int) {
        for i in 0..<arr.count{
            arr[i] = false
            
        }
        arr[tag] = true
        set_btn_ui()
    }
    
    func set_btn_ui() {
        for i in 0..<arr.count{
            if arr[i] == false{
                check_btn[i].setBackgroundImage(UIImage(named: "Ellipse 343"), for: .normal)
                back_view[i].backgroundColor = hexStringToUIColor(hex: "#F2F2F2")
                titlee[i].textColor = hexStringToUIColor(hex: "#8895A3")
                info_btn[i].setBackgroundImage(UIImage(named: "info_button-2"), for: .normal)
            }else{
                check_btn[i].setBackgroundImage(UIImage(named: "checkmark_green"), for: .normal)
                back_view[i].backgroundColor = hexStringToUIColor(hex: "#8895A3")
                titlee[i].textColor = hexStringToUIColor(hex: "#FFFFFF")
                info_btn[i].setBackgroundImage(UIImage(named: "info_button-1"), for: .normal)
            }
        }
    }
  
    @IBAction func next_levl(_ sender: UIButton) {
        self.delegte_call?.nav_VC(tag: sender.tag )
       
    }

    @IBAction func selected_btn(_ sender: UIButton){
        if sender.tag == 0{
            tag_wise_selection(tag: 0)
        } else if sender.tag == 1 {
            tag_wise_selection(tag: 1)
        }else if sender.tag == 2 {
            tag_wise_selection(tag: 2)
        }else if sender.tag == 3 {
            tag_wise_selection(tag: 3)
        } else{
            tag_wise_selection(tag: 4)
        }
    }
    

}


extension d_cook_VC: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "d_cook_CollectionCell", for: indexPath) as! d_cook_CollectionCell
        
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       return CGSize(width: 100, height: 50)
    }
}
