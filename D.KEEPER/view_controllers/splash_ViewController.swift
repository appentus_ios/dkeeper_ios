//  splash_ViewController.swift
//  D.KEEPER

//  Created by ayush pathak on 20/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.



import UIKit
import RevealingSplashView


class splash_ViewController: UIViewController {
    //    override var preferredStatusBarStyle: UIStatusBarStyle {
    //        return .lightContent
    //    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //        let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "logo_green")!, iconInitialSize: CGSize(width: 70, height: 70), backgroundImage: UIImage(named: "ibqbs7l1")!)
        //
        //
        //        self.view.addSubview(revealingSplashView)
        //
        //        revealingSplashView.duration = 1.5
        //
        //        revealingSplashView.iconColor = UIColor.red
        //        revealingSplashView.useCustomIconColor = false
        //
        //        revealingSplashView.animationType = SplashAnimationType.heartBeat
        //
        //        revealingSplashView.startAnimation(){
        ////            self.revealingLoaded = true
        //            self.setNeedsStatusBarAppearanceUpdate()
        ////            print("Completed")
        //        }
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
            if let userid = UserDefaults.standard.object(forKey: "userId") as? String{
                print(userid)
                self.viewPresent(storyBoard: "Main", viewId: "LGSideMenuController")
            }else{
                self.viewPresent(storyBoard: "Main", viewId: "walkthrough_VC" )
            }
        }
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return !UIApplication.shared.isStatusBarHidden
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.fade
    }
    
    
    
}


