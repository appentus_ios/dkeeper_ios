//
//  login_VC.swift
//  D.KEEPER
//
//  Created by Rajat Pathak on 11/07/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class login_VC: UIViewController {

    @IBOutlet weak var username_TF: UITextField!
    @IBOutlet weak var password_TF: UITextField!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initviews()
    }
    
    func initviews() {
        username_TF.layer.masksToBounds = true
        password_TF.layer.masksToBounds = true
        
        username_TF.layer.cornerRadius = 22.5
        password_TF.layer.cornerRadius = 22.5
        
        username_TF.setRightPaddingPoints(20)
        password_TF.setRightPaddingPoints(20)
    }
    
    @IBAction func login_btn(_ sender: UIButton) {
        loginHandler()
    }
    
    
    @IBAction func backc_btn(_ sender: UIButton) {
        dismiss_VC()
    }
    
    @IBAction func forgotpass_Btn(_ sender: Any) {
       viewPresent(storyBoard: "Main", viewId: "forgot_Pass_VC")
    }
    
}
extension login_VC{
    func loginHandler() {
        self.view.endEditing(true)
        if !isValidEmail(email: self.username_TF.text!){
            self.view.showError("Enter valid email address")
            return
        }
        
        if self.password_TF.text!.count < 6{
            self.view.showError("Password must be greater than 6 characters")
           return
        }
        
        loginUser()
        
    }
    
    func loginUser() {
        self.view.show_loader()
        let dict = [
            "email":self.username_TF.text!,
            "password":self.password_TF.text!,
        ]
        APIFunc.postAPI(url: kBaseUrl+kLoginByEmail, parameters: dict) { (resp) in
            self.view.hideLoader()
            if let error = resp["error"] as? Bool{
                print(error)
                self.view.showError(resp["message"] as! String)
            }else{
                let resultDict = resp
                let status = resultDict["status"] as! String
                if status == "success"{
                    self.view.showSuccess(resultDict["message"] as! String)
                    let dict = resultDict["result"] as! [String:Any]
                    let userId = "\(dict["user_id"]!)"
                    UserDefaults.standard.set(userId, forKey: "userId")
                    UserDefaults.standard.set(dict, forKey: "loginData")
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1.0) {
                        self.viewPresent(storyBoard: "Main", viewId: "LGSideMenuController")
                    }
                }else{
                    self.view.showError(resultDict["message"] as! String)
                }
            }
        }
    }
}
