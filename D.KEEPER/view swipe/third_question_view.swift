//
//  third_question_view.swift
//  D.KEEPER
//
//  Created by appentus on 7/31/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import UIKit

class third_question_view: UIView {

    @IBOutlet weak var search_TF: UITextField!
    @IBOutlet weak var right_Btn: UIButton!
    
    override func awakeFromNib() {
        super .awakeFromNib()
        border(view: search_TF, color: UIColor.init(red: 136.0/255.0, green: 149.0/255.0, blue: 163.0/255.0, alpha: 1.0), br_width: 0.5)
        shadow_on_view(view: search_TF, color: .clear, corner_radius: search_TF.frame.height/2)
        
        search_TF.setRightPaddingPoints(15)
        search_TF.setLeftPaddingPoints(15)
    }

    
 
}
