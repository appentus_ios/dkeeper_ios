//
//  overlay_View_3.swift
//  D.KEEPER
//
//  Created by appentus on 9/7/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//


import Foundation
import Koloda

private let overlayRightImageName = "veg"
private let overlayLeftImageName = "non_veg"


class overlay_View_3: OverlayView {

    @IBOutlet lazy var image_view: UIImageView! = {
        [unowned self] in
        
        var imageView = UIImageView(frame: self.bounds)
        self.addSubview(imageView)
        
        return imageView
        
        }()
    
    
    
    override var overlayState: SwipeResultDirection? {
        didSet {
            switch overlayState {
            case .left? :
                image_view.image = UIImage(named: overlayLeftImageName)
            case .right? :
                image_view.image = UIImage(named: overlayRightImageName)
            default:
                image_view.image = nil
            }
        }
    }

}
