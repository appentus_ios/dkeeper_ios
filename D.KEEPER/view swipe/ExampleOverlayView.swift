//
//  ExampleOverlayView.swift
//  KolodaView
//
//  Created by Eugene Andreyev on 6/21/15.
//  Copyright (c) 2015 CocoaPods. All rights reserved.
//

import UIKit
import Koloda

private let overlayRightImageName = "yes_1"
private let overlayLeftImageName = "no_1"

class ExampleOverlayView: OverlayView {
  
    
    @IBOutlet lazy var overlay_ImgV: UIImageView! = {
        [unowned self] in
        
        var imageView = UIImageView(frame: self.bounds)
        self.addSubview(imageView)
        
        return imageView
    }()
   
    
    
    
    override var overlayState: SwipeResultDirection? {
        didSet {
            switch overlayState {
            case .left? :
                overlay_ImgV.image = UIImage(named: overlayLeftImageName)
            case .right? :
                overlay_ImgV.image = UIImage(named: overlayRightImageName)
                default:
                overlay_ImgV.image = nil
            }
        }
    }

}
