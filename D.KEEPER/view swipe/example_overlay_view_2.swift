
//
//  example_overlay_view_2.swift
//  D.KEEPER
//
//  Created by appentus on 8/1/19.
//  Copyright © 2019 Rajat Pathak. All rights reserved.
//

import Foundation
import Koloda

private let overlayRightImageName = "yes_2"
private let overlayLeftImageName = "no_2"


class example_overlay_view_2: OverlayView {
    
    @IBOutlet lazy var image_view: UIImageView! = {
        [unowned self] in
        
        var imageView = UIImageView(frame: self.bounds)
        self.addSubview(imageView)
        
        return imageView
        
    }()
    
    
    
override var overlayState: SwipeResultDirection? {
    didSet {
        switch overlayState {
        case .left? :
            image_view.image = UIImage(named: overlayLeftImageName)
        case .right? :
            image_view.image = UIImage(named: overlayRightImageName)
        default:
            image_view.image = nil
        }
    }
}

}

